#!/bin/bash

cd /usr/src/app/plugins/$PLUGIN_DIR
echo $PLUGIN_DIR

if [ -f "test.js" ]; then
    node test.js
fi

node /usr/src/app/tools/modifyConfigurationForPlugin.js $PLUGIN_DIR key=$PLUGIN_KEY name=$PLUGIN_NAME

MAIN_SCRIPT=$(jq -r '.main' ./package.json)
exec node "$MAIN_SCRIPT"
