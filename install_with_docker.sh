#!/bin/bash

PLUGIN_DIR=${1:-tensorflow-4-1-0}
PLUGIN_KEY=${2:-plugin_key_for_pairing_to_shinobi}
PLUGIN_NAME=${3:-Tensorflow}
USE_NVIDIA=${4:-false}

echo "======================="
echo "Shinobi Plugin Install"
echo "======================="
echo "Plugin : ${PLUGIN_DIR}"
echo "|- Name : ${PLUGIN_NAME}"
echo "|- Key : ${PLUGIN_KEY}"
echo "|- NVIDIA Support: $(if [ "$USE_NVIDIA" = "true" ]; then echo "Enabled"; else echo "Disabled"; fi)"
echo "======================="
echo "You will need to manually add this key to your main configuration in Shinobi (conf.json in Shinobi directory)."
echo "!=====================!"
echo "Add the following to the \"pluginKeys\" object of your conf.json:"
echo "\"${PLUGIN_NAME}\":\"${PLUGIN_KEY}\","
echo "!=====================!"
echo "Starting in 5 seconds..."
sleep 5

mkdir -p ${HOME}/ShinobiPlugins/${PLUGIN_DIR}

# Determine which docker-compose file to use based on the fourth parameter
if [ "$USE_NVIDIA" = "true" ]; then
    cp nvidia-docker-compose.yml docker-compose-${PLUGIN_DIR}.yml
else
    cp docker-compose.yml docker-compose-${PLUGIN_DIR}.yml
fi

# Replace placeholders with actual values
sed -i "s/tensorflow-4-1-0/${PLUGIN_DIR}/g" docker-compose-${PLUGIN_DIR}.yml
sed -i "s/plugin_key_for_pairing_to_shinobi/${PLUGIN_KEY}/g" docker-compose-${PLUGIN_DIR}.yml
sed -i "s/Tensorflow/${PLUGIN_NAME}/g" docker-compose-${PLUGIN_DIR}.yml

# Run Docker Compose and show logs
docker compose -f docker-compose-${PLUGIN_DIR}.yml up -d --build
docker logs shinobi-plugin-${PLUGIN_DIR} -f
