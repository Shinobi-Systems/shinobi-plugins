# Shinobi Plugins

These plugins are released under the same license as Shinobi Pro unless otherwise stated within the plugin's directory.

# Install with Docker

This is just a wrapper for docker-compose. It utilizes the default `docker-compose.yml` and generates a new one based on the plugin you selected.

Below you can see the command to run. If you select nothing it will use `tensorflow-4-1-0`.

```
sh install_with_docker.sh $PLUGIN_DIR $PLUGIN_KEY $PLUGIN_NAME $USE_NVIDIA
```

Here is an example of selecting and daemonizing `YoloV5 PyTorch`.

```
sh install_with_docker.sh yolo-v5-pt ffff12345 BigYoloPlug true
```
Here's a breakdown of what each argument of the command is and does.

- `PLUGIN_DIR` is the folder name seen in the `plugins` folder. In our example we used `yolo-v5-pt`.
- `PLUGIN_KEY` is the pairing key for your plugin and Shinobi instance. It can be set to anything you want, they just need to match in both ends. This will be explained below.
- `PLUGIN_NAME` is the human name for your plugin. It is also used for the quick pairing setup.
- `USE_NVIDIA` is for toggling use of NVIDIA GPU for the plugin's base image. Default is `false`.

# Pairing your Plugin to Shinobi (Superuser Panel, Web UI)

1. Login to Superuser panel at `http://YOUR_SHINOBI/super`. `YOUR_SHINOBI` is your Shinobi's IP and Port.

2. Open the Configuration tab and find the `pluginKeys` object. If it does not exist you can add it. Set one of the keys as the following for parameter and value.

```
"pluginKeys": {
    "$PLUGIN_NAME": "$PLUGIN_KEY",
    "$PLUGIN_NAME2": "$PLUGIN_KEY2"
}
```

You can edit the conf.json as a regular JSON in the top bar of this tab. Quickly paste and save.

# Pairing your Plugin to Shinobi (Terminal)

1. Navigate to your Shinobi directory on your Host. By default this is `$HOME/Shinobi` for Docker installations of Shinobi. `/home/Shinobi` is default for bare metal installations.

```
cd $HOME/Shinobi
```

2. Edit the `conf.json`

```
nano conf.json
```

3. Edit `pluginKeys` object with the following additon. If `pluginKeys` does not exist you can create it.

```
"pluginKeys": {
    "$PLUGIN_NAME": "$PLUGIN_KEY",
    "$PLUGIN_NAME2": "$PLUGIN_KEY2"
}
```
