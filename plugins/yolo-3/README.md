# Yolo V3

This version of the Yolo Plugin has been officially tested with the following GPUs.

- NVIDIA RTX 3070
- NVIDIA GTX 1660
- NVIDIA GTX 1050
- NVIDIA GTX 980M
- NVIDIA GT 710

The GPUs were running with a variety of NVIDIA Driver, CUDA Toolkit, and CUDNN versions. Yolo is the most flexible Object Detector as it seems to run on almost any NVIDIA GPU we attempt it with.

CPU only Object Detection use is not recommended unless your CPU is powerful.

## Required NVIDIA Drivers for GPU Support

The "Run Installer" command does not offer Driver Installation. You must select Driver Installation manually from the Commands drop down for this plugin.

NVIDIA Drivers v515, CUDNN v8, CUDA Toolkit 11.2 Installers are offered for Ubuntu 20.04 and Rocky 9

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **"Plugin Manager"** tab.
3. In the listing select the latest **"Object Detection (Yolo V3)"** plugin to Download.
4. Skip this for CPU Only or if you already did it before. Select **"Install NVIDIA Drivers v515, CUDNN v8, CUDA Toolkit 11.2 (Ubuntu 20.04 and Rocky 9)"**
5. Hit `Run Installer` for the newly downloaded plugin.
6. Once Installed click "Enable" and restart Shinobi.
7. Reopen the Plugin Manager and run the `Test Object Detector`.
8. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)

Your test should look similar to this.

```
Network configured and loaded in 0.234758 seconds
Loading https://cdn.shinobi.video/images/test/car.jpg
Detecting upon https://cdn.shinobi.video/images/test/car.jpg
Detected Objects!
{
  x: 130.05662536621094,
  y: 0,
  width: 250.96682739257812,
  height: 390.0387878417969,
  tag: 'car',
  confidence: 65.43328857421875
}
Done https://cdn.shinobi.video/images/test/car.jpg
Loading https://cdn.shinobi.video/images/test/bear.jpg
Detecting upon https://cdn.shinobi.video/images/test/bear.jpg
No Matrices...
Done https://cdn.shinobi.video/images/test/bear.jpg
Loading https://cdn.shinobi.video/images/test/people.jpg
Detecting upon https://cdn.shinobi.video/images/test/people.jpg
Detected Objects!
{
  x: 267.1744079589844,
  y: 75.33380126953125,
  width: 187.724609375,
  height: 428.436279296875,
  tag: 'person',
  confidence: 53.21321487426758
}
Done https://cdn.shinobi.video/images/test/people.jpg
#END_PROCESS
```

[Learn more about Downloading and Installing Plugins here](https://docs.shinobi.video/plugin/install)
