process.on('uncaughtException', function (err) {
    console.error(`Uncaught Exception occured! ${new Date()}`);
    console.error(err.stack);
});
const fs = require('fs').promises;
const { spawn } = require('child_process');
const async = require('async');
const main = async () => {
    const {
        detect,
        loadModel,
    } = await require('../libs/objectDetector.js')(null, {});
    await loadModel();
    const apiEndpoint = 'http://localhost:8900';
    const VIDEO_URL = 'https://cdn.shinobi.video/videos/PlateDemo.mp4';
    const worker = async (task, callback) => {
        const currentTime = new Date().getTime()
        try {
            const detectResponse = await detect(task.frameBuffer, apiEndpoint);
            console.log(`Frame ${task.i} detected in ${(new Date().getTime() - currentTime) / 1000}s`);
            console.log(detectResponse);
        } catch (err) {
            console.error(err);
        }
        callback();
    };
    const queue = async.queue(worker, 1);
    const processFramesFromVideo = async () => {
        return new Promise(async (resolve, reject) => {
            let frameData = [];
            let i = 0;
            const ffmpeg = spawn('ffmpeg', [
                '-re',
                '-i', VIDEO_URL,
                '-f', 'image2pipe',
                '-vf', 'fps=10',  // Extract 1 frame per second (you can adjust this)
                '-c:v', 'mjpeg',
                '-'
            ]);

            ffmpeg.stdout.on('data', async (chunk) => {
                frameData.push(chunk);

                if((chunk[chunk.length-2] === 0xFF && chunk[chunk.length-1] === 0xD9)){
                    const frameBuffer = Buffer.concat(frameData);
                    frameData = [];
                    ++i;
                    const task = {
                        frameBuffer,
                        i,
                    };
                    queue.push(task);
                }
            });

            ffmpeg.on('close', (code) => {
                if (code !== 0) {
                    reject(new Error(`ffmpeg exited with code ${code}`));
                } else {
                    resolve();
                }
            });
        });
    };

    const beginTest = async () => {
        const startTime = new Date().getTime();
        await processFramesFromVideo();
        const endTime = new Date().getTime();
        const inferenceTime = (endTime - startTime) / 1000;
        console.log(`Test done in ${inferenceTime}s!`);
    };

    beginTest().then(() => {
        process.exit();
    }).catch(console.error);
}
main()
