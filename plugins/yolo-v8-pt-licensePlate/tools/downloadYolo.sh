#!/bin/bash

DIR="$(dirname "$(readlink -f "$0")")"
echo "Script is located at: $DIR"
cd "$DIR/.."
VENV_NAME="$DIR/../yolov8-venv"
VENV_PIP="$VENV_NAME/bin/pip3"
apt install python3-pip python3-venv -y
python3 -m venv $VENV_NAME

$VENV_PIP install flask===3.0.3
$VENV_PIP install ultralytics===8.3.2
$VENV_PIP install easyocr===1.7.1
$VENV_PIP install numpy===1.26.4
$VENV_PIP install antlr4-python3-runtime==4.13.2
$VENV_PIP install tomli==2.0.2
$VENV_PIP install opencv-python===4.10.0.84
$VENV_PIP install opencv-python-headless===4.10.0.84
$VENV_PIP install torch===2.4.1
$VENV_PIP install torchvision===0.19.1
$VENV_PIP install omegaconf
