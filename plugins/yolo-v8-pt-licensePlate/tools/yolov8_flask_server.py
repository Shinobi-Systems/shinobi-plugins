import torch
import cv2
import easyocr
from ultralytics import YOLO
from ultralytics.utils.plotting import Annotator, colors
from flask import Flask, request, jsonify, render_template, send_file
import numpy as np
from io import BytesIO
import argparse
import logging
from werkzeug import serving

# Initialize EasyOCR reader for English language, using GPU if available
print("Initializing EasyOCR...")
reader = easyocr.Reader(['en'], gpu=True)
print("EasyOCR initialized successfully.")

app = Flask(__name__)

def ocr_image(img, coordinates):
    x, y, w, h = int(coordinates[0]), int(coordinates[1]), int(coordinates[2]), int(coordinates[3])

    img = img[y:h, x:w]

    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    result = reader.readtext(gray)
    largest_text = ""
    largest_area = 0
    ocr_confidence = 0.0

    for res in result:
        box = res[0]
        text = res[1]
        confidence = res[2]

        width = box[1][0] - box[0][0]
        height = box[2][1] - box[0][1]
        area = width * height

        if area > largest_area:
            largest_area = area
            largest_text = text
            ocr_confidence = confidence

    return largest_text, ocr_confidence


class DetectionPredictor:

    def preprocess(self, img, device):
        img_resized = cv2.resize(img, (640, 640))
        img_resized = torch.from_numpy(img_resized).to(device)
        img_resized = img_resized.half() if device == 'cuda' else img_resized.float()
        img_resized /= 255
        img_resized = img_resized.permute(2, 0, 1)
        return img_resized

    def postprocess(self, preds, img, orig_img):
        boxes = preds[0].boxes
        if boxes is None or len(boxes) == 0:
            return []

        orig_h, orig_w = orig_img.shape[:2]
        model_h, model_w = img.shape[1:]
        processed_preds = []
        for box in boxes:
            xyxy = box.xyxy.cpu().numpy().flatten()
            xyxy[0] = xyxy[0] * (orig_w / model_w)
            xyxy[1] = xyxy[1] * (orig_h / model_h)
            xyxy[2] = xyxy[2] * (orig_w / model_w)
            xyxy[3] = xyxy[3] * (orig_h / model_h)

            conf = box.conf.item()
            cls = box.cls.item()

            processed_preds.append((xyxy, conf, cls))
        return processed_preds

    def annotate_and_return_json(self, preds, frame):
        detections = []

        for box in preds:
            xyxy = box[0]
            conf = box[1]
            cls = box[2]
            text_ocr, ocr_confidence = ocr_image(frame, xyxy)

            label = text_ocr if text_ocr else f'{int(cls)} {conf:.2f}'
            ocr_confidence = ocr_confidence if text_ocr else 0

            detections.append({
                'x': int(xyxy[0]),
                'y': int(xyxy[1]),
                'width': int(xyxy[2] - xyxy[0]),
                'height': int(xyxy[3] - xyxy[1]),
                'tag': label,
                'confidence': ocr_confidence
            })

        return detections

predictor = DetectionPredictor()

@app.route('/upload', methods=['POST'])
def upload_image():
    if 'image' not in request.files:
        return jsonify({'error': 'No image uploaded'}), 400

    file = request.files['image']
    img = np.frombuffer(file.read(), np.uint8)
    img = cv2.imdecode(img, cv2.IMREAD_COLOR)

    img_tensor = predictor.preprocess(img, device)
    preds = model(img_tensor.unsqueeze(0))

    processed_preds = predictor.postprocess(preds, img_tensor, img)
    detections = predictor.annotate_and_return_json(processed_preds, img)

    return jsonify(detections)

@app.route('/form')
def form():
    return '''
    <!doctype html>
    <title>Upload an Image</title>
    <h1>Upload new Image</h1>
    <form method=post enctype=multipart/form-data action="/upload">
      <input type=file name=image>
      <input type=submit value=Upload>
    </form>
    '''

if __name__ == '__main__':
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    ultralytics_logger = logging.getLogger('ultralytics')
    ultralytics_logger.setLevel(logging.ERROR)
    parser = argparse.ArgumentParser(description='YOLOv8 License Plate Detection Flask Server')
    parser.add_argument('--model', type=str, default='yolov8n.pt', help='Path to the YOLOv8 model file')
    parser.add_argument('--port', type=int, default=8989, help='Port for the Flask server to run on')
    args = parser.parse_args()

    device = 'cuda' if torch.cuda.is_available() else 'cpu'
    print(f"Using device: {device}")

    print(f"Loading YOLOv8 model from {args.model}")
    model = YOLO(args.model)

    app.run(host='0.0.0.0', port=args.port)
