WEIGHTS_DIR="./weights"
mkdir -p $WEIGHTS_DIR

wget -O "$WEIGHTS_DIR/yolov8s.pt" "https://cdn.shinobi.video/weights/yolov8-licenseplate/best.pt"

echo "licensePlate" > "$WEIGHTS_DIR/classes.txt"

echo "Weights download completed."
