const fs = require('fs').promises;
const fetch = require('node-fetch');
const spawn = require('child_process').spawn;
const FormData = require('form-data');
let modelProcess = null;
function cleanupModelProcess() {
    if (modelProcess) {
        console.log('Killing the Python model process...');
        modelProcess.kill('SIGINT');
    }
    process.exit();
}
process.on('SIGINT', cleanupModelProcess);
process.on('SIGTERM', cleanupModelProcess);
process.on('uncaughtException', (err) => {
    console.error('Uncaught Exception occurred:', err);
    cleanupModelProcess();
});
process.on('exit', cleanupModelProcess);

module.exports = async (s, config = {}) => {
    const weightsPath = config.weightsPath && await fs.exists(config.weightsPath) ? config.weightsPath : `${process.cwd()}/weights/yolov8s.pt`;
    const serverPath = config.serverPath || `${process.cwd()}/tools/yolov8_flask_server.py`;
    const serverPort = config.serverPort || 8989;
    function formatDetection(detection) {
        return {
            x: detection.bbox[0],
            y: detection.bbox[1],
            width: detection.bbox[2] - detection.bbox[0],
            height: detection.bbox[3] - detection.bbox[1],
            tag: classes[detection.class] || detection.class,
            confidence: detection.confidence
        };
    }

    async function detect(imageBuffer, flaskUrl = `http://localhost:${serverPort}`) {
        try {
            const formData = new FormData();
            // Append imageBuffer as a file with a proper filename
            formData.append('image', imageBuffer, { filename: `${new Date().getTime()}.jpg` });

            const response = await fetch(flaskUrl + '/upload', {
                method: 'POST',
                body: formData,
                headers: formData.getHeaders(),  // Ensures multipart form-data headers are set correctly
            });

            if (!response.ok) {
                throw new Error(`Server responded with ${response.status}: ${response.statusText}`);
            }

            const json = await response.json();
            return json;
        } catch (error) {
            console.error('Error uploading image:', error);
            throw error;
        }
    }

    function loadModel(options = {}) {
        return new Promise((resolve) => {
            console.log('Loading Model...')
            const termsToFindForResolve = ["Serving Flask app"];
            options.onData = options.onData || function(){};
            modelProcess = spawn(`${__dirname}/../yolov8-venv/bin/python3`, [serverPath, '--port', serverPort, '--model', weightsPath]);
            modelProcess.stdout.on('data', (data) => {
                const lines = data.toString();
                options.onData(lines);
            });

            modelProcess.on('close', (code) => {
                // never let it die
                console.log('Model Died!')
                setTimeout(() => {
                    loadModel(options);
                }, 5000);
            });

            modelProcess.stderr.on('data', (data) => {
                const string = data.toString();
                console.log(string)
            })
            let waitingToReadyModel = (data) => {
                const string = data.toString();
                let included = 0;
                termsToFindForResolve.forEach((term) => {
                    if(string.includes(term))++included;
                });
                if(included === termsToFindForResolve.length){
                    console.log('Loaded Model!')
                    modelProcess.stdout.off('data', waitingToReadyModel)
                    waitingToReadyModel = (data) => {
                        const string = data.toString();
                        console.log(string)
                    }
                    modelProcess.stdout.on('data', waitingToReadyModel)
                    resolve()
                }
                console.error(`Detector Server : ${data}`);
            }
            modelProcess.stdout.on('data', waitingToReadyModel);
        })
    }
    return { detect, loadModel };
}
