//
// Shinobi - Face Plugin
// Copyright (C) 2016-2025 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
const fs = require('fs');
const sharp = require('sharp');
const configPath = './conf.json';
var config = require(configPath)
var dotenv = require('dotenv').config()
var s
const {
  workerData
} = require('worker_threads');
const isWorker = workerData && workerData.ok === true;
const facesFolder = config.facesFolder || './faces'
const unknownFacesFolder = config.unknownFacesFolder || './unknownFaces'
const saveUnknownFaces = {}
if(isWorker){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
s.onCameraInit((monitorConfig, cn, tx) => {
    var groupKey = monitorConfig.ke;
    var monitorId = monitorConfig.mid;
    var monitorKey = `${groupKey}${monitorId}`
    var monitorDetails = monitorConfig.details;
    var saveThem = monitorDetails.face_save_unknown !== '0'
    saveUnknownFaces[monitorKey] = saveThem;
});
// Base Init />>
// Face - Face Recognition Init >>
var weightLocation = __dirname + '/weights'
const canvas = require('canvas')
var tfjsSuffix = ''
switch(config.tfjsBuild){
    case'gpu':
        tfjsSuffix = '-gpu'
    break;
    case'cpu':
    break;
    default:
        try{
            require(`@tensorflow/tfjs-node`)
        }catch(err){
            console.log(err)
        }
    break;
}
var tf = require(`@tensorflow/tfjs-node${tfjsSuffix}`)
faceapi = require('face-api.js')

const { createCanvas, Image, ImageData, Canvas } = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })
s.monitorLock = {}
const minConfidence = config.faceMinConfidence || 0.5
// Face - Face Recognition Init />>
var addAwaitStatements = async function(){
    s.detectObject = function(){}
    await faceapi.nets.ssdMobilenetv1.loadFromDisk(weightLocation)
    await faceapi.nets.faceLandmark68Net.loadFromDisk(weightLocation)
    await faceapi.nets.faceRecognitionNet.loadFromDisk(weightLocation)
    const faceDetectionNet = faceapi.nets.ssdMobilenetv1
    var faceDetectionOptions = new faceapi.SsdMobilenetv1Options({ minConfidence });
    var faces = {}
    var labeledDescriptors = {}
    var faceMatcher = {}
    var facesLoaded = {}
    const faceImagesIndex = {}
    const faceImagesIndexArray = {}
    var faceDescriptorRefreshTimeout
    var alreadyCompiling = {}
    var loadingUnknown = {}
    const isJpeg = (imageFile) => {
        imageFile.indexOf('.jpg') > -1 || imageFile.indexOf('.jpeg') > -1
    }
    const loadFaceGroups = () => {
        if(!fs.existsSync(facesFolder)){
            fs.mkdirSync(facesFolder);
        }
        if(!fs.existsSync(unknownFacesFolder)){
            fs.mkdirSync(unknownFacesFolder);
        }
        fs.readdirSync(facesFolder).forEach((groupKey) => {
            faces[groupKey] = fs.readdirSync(`${facesFolder}/${groupKey}`)
            // console.log(faces[groupKey])
        })
        fs.readdirSync(unknownFacesFolder).forEach((groupKey) => {
            faces[groupKey] = (faces[groupKey] || []).concat(fs.readdirSync(`${unknownFacesFolder}/${groupKey}`))
            createAllFaceDescriptors(groupKey)
        })
    }
    const createNewFaceMatcherFromLoadedFaceDescriptors = (groupKey) => {
        faceMatcher[groupKey] = null
        const cleanDescriptors = (labeledDescriptors[groupKey] || []).filter(function (el) {
          return el != null;
        });
        if(cleanDescriptors.length > 0)faceMatcher[groupKey] = new faceapi.FaceMatcher(cleanDescriptors)
    }
    const removeFaceImageFromMemory = (groupKey,personName,imageFile) => {
        const thisFaceIndex = faces[groupKey].indexOf(personName)
        if(thisFaceIndex === -1)return;
        if(faceImagesIndex[groupKey][personName]){
            faceImagesIndex[groupKey][personName][imageFile] = null
            faceImagesIndex[groupKey][personName][getFilenamePrefix(imageFile) + '.faceDescriptor'] = null
            delete(faceImagesIndex[groupKey][personName][imageFile])
            delete(faceImagesIndex[groupKey][personName][getFilenamePrefix(imageFile) + '.faceDescriptor'])
        }
        const faceDescriptors = Object.values(faceImagesIndex[groupKey][personName] || {})
        // console.log(faceImagesIndex[groupKey])
        if(faceDescriptors.length > 0){
            // console.log('faceDescriptors','make new')
            labeledDescriptors[groupKey][thisFaceIndex] = new faceapi.LabeledFaceDescriptors(
                personName,
                faceDescriptors
            )
        }else{
            // console.log('faceDescriptors','delete')
            labeledDescriptors[groupKey].splice(thisFaceIndex, 1)
        }
    }
    const getFaceFolderPrefix = (groupKey,personName) => {
        const isUnknownFace = (personName.indexOf('UNKN_') > -1)
        return (isUnknownFace ? unknownFacesFolder + '/' : facesFolder + '/') + `${groupKey}/`
    }
    const moveFaceImageInFileSystem = (options,callback) => {
        const groupKey = options.groupKey
        const oldFaceFolderPrefix = getFaceFolderPrefix(groupKey,options.personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(groupKey,options.newPersonName)
        const oldImagePath = oldFaceFolderPrefix + options.personName + '/' + options.imageFile
        const newImagePath = newFaceFolderPrefix + options.newPersonName + '/' + options.newImageFile
        const oldFaceDescriptorPath = oldFaceFolderPrefix + options.personName + '/' + getFilenamePrefix(options.imageFile) + '.faceDescriptor'
        const newFaceDescriptorPath = newFaceFolderPrefix + options.newPersonName + '/' + getFilenamePrefix(options.newImageFile) + '.faceDescriptor'
        const fileExists = fs.existsSync(oldImagePath)
        if(fileExists){
            fs.readFile(oldFaceDescriptorPath,(err,data) => {
                fs.writeFile(newFaceDescriptorPath,data,() => {
                    fs.rm(oldFaceDescriptorPath,{recursive: true},() => {
                        fs.readFile(oldImagePath,(err,data) => {
                            fs.writeFile(newImagePath,data,() => {
                                fs.rm(oldImagePath,{recursive: true},() => {
                                    callback()
                                })
                            })
                        })
                    })
                })
            })
        }else{
            callback()
        }
    }
    const moveFaceFolderInFileSystem = (options,callback) => {
        var groupKey = options.groupKey
        var personName = options.personName//.replace(/FACE_/g,'UNKN_')
        // personName = personName.indexOf('UNKN_')
        if(personName.indexOf('UNKN_') > -1){
            newPersonName = newPersonName.replace(/UNKN_/g,'FACE_')
        }else if(options.copyToUnknownFaces){
            newPersonName = newPersonName.indexOf('FACE_') > -1 ? newPersonName.replace(/FACE_/g,'UNKN_') : 'UNKN_' + newPersonName
        }
        const oldFaceFolderPrefix = getFaceFolderPrefix(groupKey,personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(groupKey,newPersonName)
        const oldFolderPath = oldFaceFolderPrefix + personName
        const newFolderPath = newFaceFolderPrefix + newPersonName
        const fileExists = fs.existsSync(oldFolderPath)
        if(fileExists){
            fs.readFile(oldFolderPath,(err,data) => {
                fs.writeFile(newFolderPath,data,() => {
                    fs.rm(oldFolderPath,() => {
                        callback()
                    })
                })
            })
        }else{
            callback()
        }
    }
    const moveFaceImageInMemory = (options) => {
        const groupKey = options.groupKey
        const personName = options.personName
        const imageFile = options.imageFile
        const newPersonName = options.newPersonName
        const newImageFile = options.newImageFile
        if(!faces[groupKey])faces[groupKey] = []
        const oldFaceIndex = faces[groupKey].indexOf(personName)
        if(oldFaceIndex === -1)return;
        if(faceImagesIndex[groupKey][personName]){
            const imageFileFaceDescriptor = getFilenamePrefix(imageFile) + '.faceDescriptor'
            try{
                loadImageFaceDescriptor(groupKey,newPersonName,newImageFile, () => {
                    faceImagesIndex[groupKey][personName][imageFile] = null
                    faceImagesIndex[groupKey][personName][imageFileFaceDescriptor] = null
                    delete(faceImagesIndex[groupKey][personName][imageFile])
                    delete(faceImagesIndex[groupKey][personName][imageFileFaceDescriptor])
                    const faceDescriptors = Object.values(faceImagesIndex[groupKey][personName] || {})
                    // console.log(faceImagesIndex[groupKey])
                    if(faceDescriptors.length > 0){
                        // console.log('faceDescriptors','make new')
                        labeledDescriptors[groupKey][oldFaceIndex] = new faceapi.LabeledFaceDescriptors(
                            personName,
                            faceDescriptors
                        )
                    }else{
                        // console.log('labeledDescriptors','delete',labeledDescriptors[groupKey] ? labeledDescriptors[groupKey].length : 0)
                        labeledDescriptors[groupKey] ? labeledDescriptors[groupKey].splice(oldFaceIndex, 1) : null
                        // console.log('labeledDescriptors','delete complete',labeledDescriptors[groupKey] ? labeledDescriptors[groupKey].length : 0)
                    }
                    buildFaceMatcherLabelFromDescriptors(groupKey,newPersonName)
                    createNewFaceMatcherFromLoadedFaceDescriptors(groupKey)
                })
            }catch(err){
                console.log(err)
            }
        }
    }
    const moveFaceFolderInMemory = (options,callback) => {
        const groupKey = options.groupKey
        const personName = options.personName
        const newPersonName = options.newPersonName
        const oldFaceFolderPrefix = getFaceFolderPrefix(groupKey,personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(groupKey,newPersonName)
        fs.readdir(oldFaceFolderPrefix + personName,(err,files) => {
            if(files){
                var fileList = []
                files.forEach((fileName) => {
                    if(isJpeg(fileName)){
                        fileList.push(fileName)
                        const faceOptions = {
                            groupKey: groupKey,
                            personName: personName,
                            imageFile: fileName,
                            newPersonName: newPersonName,
                            newImageFile: fileName
                        }
                        moveFaceImageInMemory(faceOptions)
                    }
                })
            }
            callback(fileList)
        })

    }
    const getFilenamePrefix = (imageFile) => {
        const fileParts = imageFile.split('.')
        delete(fileParts[fileParts.length - 1])
        const fileName = fileParts.filter((value) => {return !!value}).join('.')
        return fileName
    }
    const writeFaceDescriptorFile = (groupKey,personName,faceDescriptor,fileName,callback) => {
        // console.log(groupKey,personName,fileName)
        const faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}/${groupKey}`
        const fileNamePrefix = getFilenamePrefix(fileName)
        const fileNameDescriptor = `${fileNamePrefix}.faceDescriptor`
        fs.writeFile(`${faceFolder}/${personName}/${fileNameDescriptor}`,faceToArrayString(faceDescriptor),'utf8',(err) => {
            if(err){
                console.log(`Failed to Save Face Descriptor`, new Date())
                console.log(err)
                console.log(new Error())
            }
            if(callback)callback(err,fileNameDescriptor)
        })
    }
    const loadImageFaceDescriptor = (groupKey,personName,imageFile,callback,parsedDescriptor) => {
            const faceDescriptorFilename = getFilenamePrefix(imageFile) + '.faceDescriptor'
            if(!faceImagesIndex[groupKey])faceImagesIndex[groupKey] = {};
            if(!faceImagesIndex[groupKey][personName])faceImagesIndex[groupKey][personName] = {};
            if(faceImagesIndex[groupKey][personName][faceDescriptorFilename])return;
            const faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}/${groupKey}/${personName}/`
            const getFromFile = () => {
                faceImagesIndex[groupKey][personName][faceDescriptorFilename] = arrayStringToFace(fs.readFileSync(faceFolder + faceDescriptorFilename))
                if(callback)callback()
            }
            if(parsedDescriptor){
                faceImagesIndex[groupKey][personName][faceDescriptorFilename] = parsedDescriptor
                if(callback)callback()
            }else if(imageFile.indexOf('.faceDescriptor') > -1){
                getFromFile()
            }else{
                fs.stat(faceFolder + faceDescriptorFilename,(err,stats) => {
                    if(stats){
                        getFromFile()
                    }else{
                        var image = new Image;
                        image.onload = function() {
                            faceapi
                              .detectSingleFace(image)
                              .withFaceLandmarks()
                              .withFaceDescriptor()
                              .then((singleResult) => {
                                  if (!singleResult) {
                                      const error = `no faces found in image ${imageFile}`
                                      if(callback)callback(error)
                                      return
                                  }
                                  faceImagesIndex[groupKey][personName][faceDescriptorFilename] = singleResult.descriptor
                                  writeFaceDescriptorFile(groupKey,personName,singleResult.descriptor,imageFile)
                                  if(callback)callback()
                              })
                              .catch((error) => {
                                  if(callback)callback(error)
                              })
                        }
                        image.src = fs.readFileSync(faceFolder + imageFile)
                    }
                })
            }
    }
    const buildFaceMatcherLabelFromDescriptors = (groupKey,personName) => {
        const thisFaceIndex = faces[groupKey].indexOf(personName)
        if(!labeledDescriptors[groupKey])labeledDescriptors[groupKey] = []
        if(!faceImagesIndexArray[groupKey])faceImagesIndexArray[groupKey] = {}
        faceImagesIndexArray[groupKey][personName] = Object.keys(faceImagesIndex[groupKey][personName])
        labeledDescriptors[groupKey][thisFaceIndex] = new faceapi.LabeledFaceDescriptors(
            personName,
            Object.values(faceImagesIndex[groupKey][personName])
        )
    }
    const createAllFaceDescriptors = (groupKey,forceRecompilation,callback) => {
        if(alreadyCompiling[groupKey])return console.log('Blocked Recompilation...');
        alreadyCompiling[groupKey] = true
        // console.log('Starting Recompilation...')
        const doRecompilation = forceRecompilation && forceRecompilation.length > 0
        facesLoaded[groupKey] = 0
        const faceGroup = faces[groupKey] || []
        const completeFaceCheck = () => {
            ++facesLoaded[groupKey]
            // console.log('facesLoaded ---> +1',facesLoaded[groupKey])
            if(facesLoaded[groupKey] >= faceGroup.length){
                // console.log('completeFaceCheck ---> createNewFaceMatcherFromLoadedFaceDescriptors')
                createNewFaceMatcherFromLoadedFaceDescriptors(groupKey)
                setTimeout(() => {
                    alreadyCompiling[groupKey] = false;
                },1500)
                if(callback){
                    // console.log('completeFaceCheck ---> callback')
                    callback()
                }
            }
        }
        if(faceGroup.length === 0)completeFaceCheck()
        // console.log('faces',faces)
        faceGroup.forEach(function(personName){
            var thisFaceIndex = faceGroup.indexOf(personName)
            if(thisFaceIndex === -1){
                thisFaceIndex = faceGroup.length
                faceGroup.push(personName)
            }
            if(!faceImagesIndex[groupKey])faceImagesIndex[groupKey] = {}
            if(!faceImagesIndex[groupKey][personName])faceImagesIndex[groupKey][personName] = {}
            var faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}/${groupKey}/` + personName + '/'
            // console.log('createAllFaceDescriptors',groupKey,personName,faceFolder)
            var imageList = []
            var foundImages = []
            var foundChange = false
            try{
                imageList = fs.readdirSync(faceFolder)
            }catch(err){

            }
            // console.log(`Number of Images : ${imageList.map((filename) => {return !filename.endsWith('.faceDescriptor')}).length}`)
            imageList.forEach(function(imageFile,number){
                const faceDescriptorFile = `${getFilenamePrefix(imageFile)}.faceDescriptor`
                if(!faceImagesIndex[groupKey][personName][imageFile] && !faceImagesIndex[groupKey][personName][faceDescriptorFile]){
                    if(isJpeg(imageFile)){
                        foundChange = true
                        try{
                            if(fs.statSync(faceFolder + faceDescriptorFile)){
                                foundImages.push(faceDescriptorFile)
                            }else{
                                foundImages.push(imageFile)
                            }
                        }catch(err){
                            foundImages.push(imageFile)
                        }
                    }else if(imageFile === faceDescriptorFile){
                        foundChange = true
                        foundImages.push(imageFile)
                    }
                }
            })
            // console.log('foundChange : ',personName, foundChange)
            if(!foundChange){
                // console.log('No Changes Found for : ' + personName)
                completeFaceCheck()
            }else{
                // console.log('Loading : ' + personName)
                var faceChanged = false
                foundImages.forEach(function(imageFile,number){
                    const completeImageCheck = () => {
                        if(number === foundImages.length - 1){
                            // console.log('Loaded : ' + personName)
                            const newKeys = Object.keys(faceImagesIndexArray[groupKey]?.[personName] ? faceImagesIndexArray[groupKey][personName] : [])
                            if(faceChanged || faceImagesIndexArray[groupKey][personName] !== newKeys || doRecompilation && forceRecompilation.indexOf(personName)){
                                buildFaceMatcherLabelFromDescriptors(groupKey,personName)
                            }
                            completeFaceCheck()
                        }
                    }
                    if(!faceImagesIndex[groupKey][personName][imageFile]){
                        faceChanged = true;
                        loadImageFaceDescriptor(groupKey,personName,imageFile,(err) => {
                            completeImageCheck()
                        })
                    }else{
                        completeImageCheck()
                    }
                })
            }
        })
    }
    const recompileFaceDescriptors = (groupKey,forceRecompilation,noCancellation,callback) => {
        if(faceDescriptorRefreshTimeout){
            if(noCancellation){
                // console.log('No Cancelled')
                return
            }
            // console.log('Cancelling previous recompilation request...')
        }
        // console.log('Recompiling Face Descriptors...')
        clearTimeout(faceDescriptorRefreshTimeout)
        faceDescriptorRefreshTimeout = setTimeout(()=>{
            faceDescriptorRefreshTimeout = null
            createAllFaceDescriptors(groupKey,forceRecompilation,callback)
        },10000)
    }
    const faceToArrayString = (faceDescriptor) => {
        return JSON.stringify(Array.from(faceDescriptor))
    }
    const arrayStringToFace = (faceString) => {
        return Float32Array.from(JSON.parse(faceString))
    }
    const foundUnknownFace = async (groupKey, frameBuffer,faceDescriptor,matrix) => {
        if(alreadyCompiling[groupKey] === true || faceDescriptorRefreshTimeout || loadingUnknown[groupKey] === true)return;
        loadingUnknown[groupKey] = true
        const currentTime = (new Date()).getTime()
        const newFaceId = `UNKN_${currentTime}`
        if(!faces[groupKey])faces[groupKey] = []
        const faceGroup = faces[groupKey]
        faceGroup.push(newFaceId)
        try{
            const facePath = `${unknownFacesFolder}/${groupKey}/${newFaceId}`
            fs.mkdir(`${facePath}`,{recursive: true},async (err) => {
                writeFaceDescriptorFile(groupKey,newFaceId,faceDescriptor,`${currentTime}.newUnknownFace`,(err,faceDescriptorFilename) => {
                    if(!err){
                        loadImageFaceDescriptor(groupKey,newFaceId,faceDescriptorFilename, () => {
                            buildFaceMatcherLabelFromDescriptors(groupKey,newFaceId)
                            createNewFaceMatcherFromLoadedFaceDescriptors(groupKey)
                            loadingUnknown[groupKey] = false
                        },faceDescriptor)
                    }
                })
                var objectCutOut = await sharp(frameBuffer).extract({
                    width: parseInt(matrix.width),
                    height: parseInt(matrix.height),
                    left: parseInt(matrix.x),
                    top: parseInt(matrix.y)
                }).toBuffer()
                fs.writeFile(`${facePath}/${currentTime}.jpg`,objectCutOut,(err) => {
                    if(err)console.log(err)
                })
            })
        }catch(err){
            console.log(err)
        }
    }
    var startDetecting = function(){
        // console.log('Ready to Detect Faces')
        s.detectObject = function(buffer,d,tx,frameLocation){
            const groupKey = d.ke
            var detectStuff = function(frameBuffer,callback){
                try{
                    var startTime = new Date()
                    var image = new Image;
                    if(alreadyCompiling[groupKey]){
                        return
                    }
                    image.onload = async function() {
                        faceapi.detectAllFaces(image, faceDetectionOptions)
                        .withFaceLandmarks()
                        .withFaceDescriptors()
                        .then((data) => {
                            if(data && data[0]){
                                const groupFaceMatcher = faceMatcher[d.ke]
                                if(groupFaceMatcher){
                                    data.forEach(fd => {
                                        var bestMatch = groupFaceMatcher.findBestMatch(fd.descriptor)
                                        fd.detection.tag = bestMatch.toString()
                                    })
                                }
                                var endTime = new Date()
                                var matrices = []
                                const monitorKey = `${d.ke}${d.id}`;
                                const { width: imgWidth, height: imgHeight } = s.monitorInfo[monitorKey];
                                data.forEach(function(box){
                                    var v = box.detection || box._detection
                                    var tag,confidence
                                    if(v.tag){
                                        var split = v.tag.split('(')
                                        tag = split[0].trim()
                                        if(tag === 'unknown'){
                                            tag = 'UNKNOWN FACE'
                                            if(saveUnknownFaces[monitorKey])foundUnknownFace(d.ke, frameBuffer,box.descriptor,v._box)
                                        }
                                        if(split[1]){
                                            confidence = split[1].replace(')','')
                                        }else{
                                            confidence = v._score
                                        }
                                    }else{
                                        tag = 'UNKNOWN FACE'
                                        confidence = v._score
                                        if(saveUnknownFaces[monitorKey])foundUnknownFace(d.ke, frameBuffer,box.descriptor,v._box)
                                    }
                                    matrices.push({
                                      id: tag,
                                      x: v._box.x,
                                      y: v._box.y,
                                      width: v._box.width,
                                      height: v._box.height,
                                      tag: tag,
                                      confidence: v._score,
                                    })
                                })
                                if(matrices.length > 0){
                                    tx({
                                        f:'trigger',
                                        id:d.id,
                                        ke:d.ke,
                                        details:{
                                            plug:config.plug,
                                            name:'face',
                                            reason:'object',
                                            matrices:matrices,
                                            imgHeight: imgHeight,
                                            imgWidth: imgWidth,
                                            ms: endTime - startTime
                                        },
                                    })
                                }
                            }
                        })
                        .catch((err) => {
                            console.log(err)
                        })
                    }
                    image.src = frameBuffer;
                }catch(err){
                    s.monitorLock[d.ke+d.id] = false
                    console.log(err)
                }
            }
            if(frameLocation){
                fs.readFile(frameLocation,function(err,buffer){
                    if(!err){
                        detectStuff(buffer)
                    }
                    fs.unlink(frameLocation,function(){

                    })
                })
            }else{
                detectStuff(buffer)
            }
        }
    }

    // add websocket handlers
    var sendData = () => {

    }
    const onSocketEvent = (d) => {
        const groupKey = d.groupKey || d.ke;
        switch(d.f){
            case'moveFaceFolder':
                const moveFaceFolderOptions = {
                    groupKey: groupKey,
                    personName: d.personName,
                    newPersonName: d.newPersonName,
                }
                moveFaceFolderInFileSystem(moveFaceFolderOptions,() => {
                    moveFaceFolderInMemory(moveFaceFolderOptions,(fileList) => {
                        sendData('callback',d.callbackId)
                    })
                })
            break;
            case'moveFaceImage':
                const moveFaceOptions = {
                    groupKey: groupKey,
                    personName: d.personName,
                    imageFile: d.imageFile,
                    newPersonName: d.newPersonName,
                    newImageFile: d.newImageFile,
                }
                moveFaceImageInFileSystem(moveFaceOptions,() => {
                    moveFaceImageInMemory(moveFaceOptions)
                    recompileFaceDescriptors(groupKey,null,true)
                })
            break;
            case'deleteFaceImage':
                removeFaceImageFromMemory(groupKey,d.personName,d.imageFile)
            break;
            case'recompileFaceDescriptors':
                // console.log('recompileFaceDescriptors')
                // console.log(d.faces)
                // console.log(d.unknownFaces)
                // console.log(groupKey)
                faces[groupKey] = (d.faces || []).concat(d.unknownFaces || [])
                recompileFaceDescriptors(groupKey,d.forceRecompile,true)
            break;
        }
    }
    if(isWorker){
        sendData = (type,data) => {
            parentPort.postMessage([type,data])
        }
        const {
            parentPort
        } = require('worker_threads');
        parentPort.on('message',onSocketEvent)
    }else{
        // add websocket handlers
        const io = s.getWebsocket()
        if(config.mode === 'host'){
            io.on('connection', function (cn) {
                sendData = (type,data) => {
                    cn.emit(type,data)
                }
                cn.on('f',onSocketEvent)
            })
        }else{
            sendData = (type,data) => {
                io.emit(type,data)
            }
            io.on('f',onSocketEvent)
        }
    };
    loadFaceGroups()
    startDetecting()
}
addAwaitStatements()
