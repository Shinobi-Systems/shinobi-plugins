//
// Shinobi - ZoneMinder MLAPI Plugin
// Copyright (C) 2023 Moe Alam, Shinobi Systems
//
// Base Init >>
const fs = require('fs');
const config = require('./conf.json')
const fetch = require('node-fetch');
const FormData = require('form-data');
var s
const {
		workerData
	} = require('worker_threads');

if(workerData && workerData.ok === true){
	try{
		s = require('../pluginWorkerBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			s = require('./pluginWorkerBase.js')(__dirname,config)
		}catch(err){
			console.log(err)
			return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
		}
	}
}else{
	try{
		s = require('../pluginBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			s = require('./pluginBase.js')(__dirname,config)
		}catch(err){
			console.log(err)
			return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
		}
	}
	try{
		s = require('../pluginBase.js')(__dirname,config)
	}catch(err){
		console.log(err)
		try{
			const {
				haltMessage,
				checkStartTime,
				setStartTime,
			} = require('../pluginCheck.js')

			if(!checkStartTime()){
				console.log(haltMessage,new Date())
				s.disconnectWebSocket()
				return
			}
			setStartTime()
		}catch(err){
			console.log(`pluginCheck failed`)
		}
	}

}
// Base Init />>

const zmMlApiUser = config.zmMlApiUser || 'pp';
const zmMlApiPassword = config.zmMlApiPassword || 'abc123';
const baseUrl = config.zmMlapiEndpoint || "http://localhost:5000/api/v1";
let authToken = null;

async function getAuthToken() {
    const response = await fetch(baseUrl + '/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username: zmMlApiUser, password: zmMlApiPassword })
    });

    const data = await response.json();
    authToken = data.access_token;

    if (!authToken) {
        console.error("Error retrieving access token", data);
        return process.exit();
    }
}

function zmmlapiRequest(d, frameBuffer) {
    return new Promise((resolve, reject) => {
        let body = new FormData();
        body.append('file', frameBuffer, {
            filename: 'current_frame.jpg',
            contentType: 'image/jpeg'
        });

        fetch(baseUrl + '/detect/object', {
            method: 'POST',
            headers: {
                "Authorization": `Bearer ${authToken}`
            },
            body: body
        })
            .then(res => res.json())
            .then((json) => {
                let predictions = [];
                try {
                    predictions = json || [];
                } catch (err) {
                    console.log(json, err, body);
                }
                resolve(predictions);
            })
            .catch((err) => {
                console.log(err);
                resolve([]); // resolve with empty array in case of error
            });
    });
}

getAuthToken().then(() => {
	s.detectObject = async function(frameBuffer, d, tx) {
	    const timeStart = new Date();
	    const predictions = await zmmlapiRequest(d, frameBuffer);
	    if (predictions.length > 0) {
	        const mats = [];
	        predictions.forEach(function(item) {
	            const bbox = item.box;
	            const label = item.type;
	            if (bbox && bbox.length === 4) {
	                const x_min = bbox[0];
	                const y_min = bbox[1];
	                const x_max = bbox[2];
	                const y_max = bbox[3];
	                const width = x_max - x_min;
	                const height = y_max - y_min;
	                mats.push({
	                    x: x_min,
	                    y: y_min,
	                    width: width,
	                    height: height,
	                    tag: label,
	                    confidence: parseFloat(item.confidence),
	                });
	            }
	        });

	        const isObjectDetectionSeparate = d.mon.detector_pam === '1' && d.mon.detector_use_detect_object === '1';
	        const width = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y);
	        const height = parseFloat(isObjectDetectionSeparate && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x);

	        tx({
	            f: 'trigger',
	            id: d.id,
	            ke: d.ke,
	            details: {
	                plug: config.plug,
	                name: `zmmlapi`,
	                reason: 'object',
	                matrices: mats,
	                imgHeight: width,
	                imgWidth: height,
	            },
	            frame: frameBuffer
	        });
	    }
	};
}).catch((err) => {
	console.log(err)
});
