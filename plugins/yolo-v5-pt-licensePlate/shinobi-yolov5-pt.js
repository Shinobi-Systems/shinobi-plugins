//
// Shinobi - Yolo V5 (pt) Plugin
// Copyright (C) 2023 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
var fs = require('fs').promises;
var config = require('./conf.json')
var s
const {
  workerData
} = require('worker_threads');
if(workerData && workerData.ok === true){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
// Base Init />>
async function main(){
    const {
        detect,
        loadModel,
    } = await require('./libs/objectDetector.js')(s,config);
    await loadModel();
    const apiEndpoint = config.apiEndpoint || 'http://localhost:8989';
    s.detectObject = async function(buffer,d,tx,frameLocation,callback){
        const timeStart = new Date()
        try{
            const detections = await detect(buffer, apiEndpoint);
            if(detections.length > 0){
                var isObjectDetectionSeparate = d.mon.detector_use_detect_object === '1'
                var height = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
                var width = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)
                tx({
                    f: 'trigger',
                    id: d.id,
                    ke: d.ke,
                    details: {
                        plug: config.plug,
                        name: 'yolov5-pt',
                        reason: 'object',
                        matrices: detections,
                        imgWidth: width,
                        imgHeight: height,
                        time: (new Date()) - timeStart
                    },
                    frame: buffer
                })
            }
        }catch(error){
            console.error(`await detect`,error);
        }
        callback()
    }
}
main()
