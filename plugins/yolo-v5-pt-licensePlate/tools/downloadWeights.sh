WEIGHTS_DIR="./weights"
mkdir -p $WEIGHTS_DIR

wget -O "$WEIGHTS_DIR/yolov5s.pt" "https://cdn.shinobi.video/weights/yolov5-licenseplate/yolov5s.pt"

echo "licensePlate" > "$WEIGHTS_DIR/classes.txt"

echo "Weights download completed."
