#!/usr/bin/env bash
set -e

DIR="$(dirname "$(readlink -f "$0")")"
echo "Script is located at: $DIR"
cd "$DIR/.."
VENV_NAME="$DIR/../yolov5-venv"
VENV_PIP="$VENV_NAME/bin/pip3"
apt install python3-pip python3-venv -y
python3 -m venv $VENV_NAME


if [ ! -d "yolov5" ]; then
    git clone https://github.com/ultralytics/yolov5
fi
cd yolov5
echo "$VENV_PIP install -r requirements.txt"
$VENV_PIP install -r requirements.txt
$VENV_PIP install easyocr

cd $DIR

$VENV_PIP install protobuf
$VENV_PIP install onnx
$VENV_PIP install onnxruntime-gpu
$VENV_PIP install flask
cp tools/yolov5_flask_server.py yolov5/yolov5_flask_server.py
