import sys
import argparse
from flask import Flask, request, jsonify
import torch
import easyocr
import cv2
from models.common import DetectMultiBackend
from utils.general import non_max_suppression, scale_boxes, xyxy2xywh, check_img_size
from utils.torch_utils import select_device, smart_inference_mode
from PIL import Image
import numpy as np
import logging
from werkzeug import serving

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--port', type=int, default=8989, help='port number')
parser.add_argument('--weights', type=str, default='weights/yolov5s.pt', help='model path')
parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
parser.add_argument('--imgszw', type=int, default=640, help='inference width (pixels)')
parser.add_argument('--imgszh', type=int, default=480, help='inference height (pixels)')
parser.add_argument('--conf-thres', type=float, default=0.46, help='confidence threshold')
parser.add_argument('--iou-thres', type=float, default=0.4, help='NMS IoU threshold')
parser.add_argument('--classes', nargs='+', type=int, help='filter by class')
parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
parser.add_argument('--max-det', type=int, default=1000, help='maximum detections per image')
parser.add_argument('--classes-file', type=str, default='weights/classes.txt', help='Path to classes.txt file')
parser.add_argument('--datayaml', type=str, default='data/coco128.yaml', help='dataset.yaml path')
args = parser.parse_args()

print("Initializing EasyOCR...")
reader = easyocr.Reader(['en'], gpu=True)
print("EasyOCR initialized successfully.")

app = Flask(__name__)

def ocr_image(img, coordinates):
    x1, y1, x2, y2 = map(int, coordinates)
    img_crop = img[y1:y2, x1:x2]
    gray = cv2.cvtColor(img_crop, cv2.COLOR_RGB2GRAY)
    result = reader.readtext(gray)

    largest_text = ""
    largest_area = 0
    ocr_confidence = 0.0

    for res in result:
        (box, text, confidence) = res
        width = box[1][0] - box[0][0]
        height = box[3][1] - box[0][1]
        area = width * height

        if area > largest_area:
            largest_area = area
            largest_text = text
            ocr_confidence = confidence

    return largest_text, ocr_confidence

def annotate_and_return_json(preds, frame, names):  # Removed 'self', added 'names'
    detections = []
    for box in preds:
        xyxy = box[0]
        conf = box[1]
        cls = box[2]
        text_ocr, ocr_confidence = ocr_image(frame, xyxy)

        label = text_ocr if text_ocr else f'{names[int(cls)]} {conf:.2f}'
        ocr_confidence = ocr_confidence if text_ocr else 0

        detections.append({
            'x': int(xyxy[0]),
            'y': int(xyxy[1]),
            'width': int(xyxy[2] - xyxy[0]),
            'height': int(xyxy[3] - xyxy[1]),
            'tag': label,
            'confidence': float(ocr_confidence)
        })
    return detections

# Load class names
with open(args.classes_file, 'r') as f:
    names = [line.strip() for line in f.readlines()]

# Load the model
device = select_device(args.device)
model = DetectMultiBackend(args.weights, device=device, dnn=False, data=args.datayaml, fp16=False)
stride, names, pt = model.stride, model.names, model.pt
imgsz = check_img_size((args.imgszw,args.imgszh), s=stride)


@app.route('/detect', methods=['POST'])
@smart_inference_mode()
def detect():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'}), 400

    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No selected file'}), 400

    image = Image.open(file.stream).convert('RGB')
    img = np.array(image)
    im = torch.from_numpy(img).permute(2, 0, 1).to(model.device)
    im = im.half() if model.fp16 else im.float()
    im /= 255.0
    if len(im.shape) == 3:
        im = im[None]

    # Inference
    pred = model(im, augment=False, visualize=False)
    pred = non_max_suppression(pred, args.conf_thres, args.iou_thres, args.classes, args.agnostic_nms, args.max_det)

    detections = []
    for det in pred:
        if det is not None and len(det):
            det = det.to('cpu')
            # Scale boxes to original image size
            det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], img.shape[:2]).round()  # Fixed shape
            boxes = []
            for *xyxy, conf, cls in det:
                boxes.append((xyxy, conf.item(), cls.item()))  # Convert tensors to scalars
            img_detections = annotate_and_return_json(boxes, img, names)  # Pass names
            detections.extend(img_detections)

    return jsonify(detections)

if __name__ == '__main__':
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    app.run(host='0.0.0.0', port=args.port)
