const fs = require('fs').promises;
const fetch = require('node-fetch');
const spawn = require('child_process').spawn;
const FormData = require('form-data');
module.exports = async (s,config) => {
    const weightsPath = config.weightsPath && await fs.exists(config.weightsPath) ? config.weightsPath : `${process.cwd()}/weights/yolov5s.pt`;
    const serverPath = config.serverPath || `${process.cwd()}/yolov5/yolov5_flask_server.py`;
    const serverPort = config.serverPort || 8989;
    const classesFilePath = config.classesPath || `${process.cwd()}/weights/classes.txt`;
    const classes = (await fs.readFile(classesFilePath,'utf8')).split('\n');
    let modelProcess = null;

    async function detect(imageBuffer, flaskUrl) {
        try {
            const formData = new FormData();
            formData.append('file', imageBuffer, { filename: `${new Date().getTime()}.jpg` });

            const response = await fetch(flaskUrl + '/detect', {
                method: 'POST',
                body: formData,
                headers: formData.getHeaders(),
            });

            if (!response.ok) {
                throw new Error(`Server responded with ${response.status}: ${response.statusText}`);
            }
            const json = await response.json();
            return json;
        } catch (error) {
            console.error('Error uploading image:', error);
            throw error;
        }
    }

    function loadModel(options = {}) {
        return new Promise((resolve) => {
            console.log('Loading Model...')
            options.onData = options.onData || function(){};
            modelProcess = spawn(`${__dirname}/../yolov5-venv/bin/python3`, [serverPath, '--port', serverPort, '--weights', weightsPath]);
            let onReady = (data) => {
                const lines = data.toString();
                if(lines.indexOf('Serving Flask app') > -1){
                    console.log('Loaded Model!')
                    modelProcess.stdout.off('data', onReady)
                    resolve();
                }else if(lines.indexOf('Traceback') > -1){
                    console.error('Failed to Load Model')
                    modelProcess.stdout.off('data', onReady)
                    resolve();
                }
            }
            modelProcess.stdout.on('data', onReady);
            modelProcess.stdout.on('data', (data) => {
                const lines = data.toString();
                console.log(lines);
                options.onData(lines);
            });

            modelProcess.on('close', (code) => {
                // never let it die
                console.log('Model Died!')
                setTimeout(() => {
                    loadModel(options);
                }, 5000);
            });

            modelProcess.stderr.on('data', (data) => {
                console.error(`Detector Server : ${data}`);
            });
        })
    }


    return { detect, loadModel };
}
