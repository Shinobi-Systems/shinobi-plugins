#!/bin/bash
DIR=$(dirname $0)
echo "Do not attempt to use this Installer on ARM-based CPUs."
echo "Removing existing Tensorflow Node.js modules..."
rm -rf $DIR/node_modules
if ! command -v yarn &> /dev/null
then
	npm install yarn -g
fi

if command -v yum > /dev/null 2>&1; then
	sudo yum install pixman-devel cairo-devel pango-devel libjpeg-turbo-devel make g++ gcc -y
fi
if command -v apt > /dev/null 2>&1; then
    sudo apt update -y
    sudo apt install libpixman-1-dev libcairo2-dev libpango1.0-dev libjpeg8-dev libgif-dev -y
fi

installGpuFlag=false
dontCreateKeyFlag=false

while [ ! $# -eq 0 ];
	do
		case "$1" in
			--gpu)
				installGpuFlag=true
				exit
				;;
			--dont-create-key)
				dontCreateKeyFlag=true
				exit
				;;
		esac
	shift
done
nonInteractiveFlag=false
if [ "$installGpuFlag" = true ]; then
	nonInteractiveFlag=true
fi

manualInstallRequirements() {
	npm install
}

installGpuRoute() {
	installGpuFlag=true
	manualInstallRequirements
	npm install @tensorflow/tfjs-node-gpu@1.7.3 @tensorflow/tfjs-node@1.7.3
}

installNonGpuRoute() {
	manualInstallRequirements
	npm install @tensorflow/tfjs-node@1.7.3
}

runRebuildCpu() {
	npm rebuild @tensorflow/tfjs-node --build-addon-from-source --unsafe-perm --verbose
}

runRebuildGpu() {
	npm rebuild @tensorflow/tfjs-node --build-addon-from-source --unsafe-perm --verbose
	npm rebuild @tensorflow/tfjs-node-gpu --build-addon-from-source --unsafe-perm --verbose
}

if [ "$nonInteractiveFlag" = false ]; then
	# echo "Shinobi - Do you want to install TensorFlow.js with GPU support? "
	# echo "You can run this installer again to change it."
	# echo "(y)es or (N)o"
	# read nodejsinstall
	# if [ "$nodejsinstall" = "y" ] || [ "$nodejsinstall" = "Y" ]; then
		installGpuRoute
	# else
	# 	installNonGpuRoute
	# fi
else
	if [ "$installJetsonFlag" = true ]; then
		installJetson
		armAfterInstall
	fi

	if [ "$installArmFlag" = true ]; then
		installArm
		armAfterInstall
	fi

	if [ "$installGpuFlag" = true ]; then
		installGpuRoute
	else
		installNonGpuRoute
	fi
fi

if [ ! -e "$DIR/weights" ]; then
	sh INSTALL-face.sh
fi

# npm install @tensorflow/tfjs-node-gpu@2.7.0
# npm audit fix --force
if [ "$installGpuFlag" = true ]; then
	runRebuildGpu
else
	runRebuildCpu
fi
if [ ! -e "./conf.json" ]; then
	dontCreateKeyFlag=false
    echo "Creating conf.json"
    sudo cp conf.sample.json conf.json
else
    echo "conf.json already exists..."
fi

if [ "$dontCreateKeyFlag" = false ]; then
	tfjsBuildVal="cpu"
	if [ "$installGpuFlag" = true ]; then
		tfjsBuildVal="gpu"
	fi
	currentFolderName=${PWD##*/}
	absolutePath="$(cd "$(dirname "$1")"; pwd)/$(basename "$1")"
	echo "Adding Random Plugin Key to Main Configuration"
	node $DIR/../../tools/modifyConfigurationForPlugin.js $currentFolderName key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}') tfjsBuild=$tfjsBuildVal facesFolder="$absolutePath/../../faces"
	node $DIR/../../tools/modifyConfiguration.js facesFolder="$absolutePath/../../faces"
fi
echo "Installing Face Manager customAutoLoad Module..."
faceManagerLocation=$DIR/../../libs/customAutoLoad/faceManagerCustomAutoLoadLibrary
rm -rf $faceManagerLocation
sudo cp -r $DIR/faceManagerCustomAutoLoadLibrary $faceManagerLocation
echo "TF_FORCE_GPU_ALLOW_GROWTH=true" > "$DIR/.env"
echo "#CUDA_VISIBLE_DEVICES=0,2" >> "$DIR/.env"
