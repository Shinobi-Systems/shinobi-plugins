//
// Shinobi - Coral Edge TPU C++ Plugin
// Copyright (C) 2023 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
var fs = require('fs').promises;
var config = require('./conf.json')
var s
const {
  workerData
} = require('worker_threads');
if(workerData && workerData.ok === true){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
// Base Init />>
const {
    loadModel,
    detect
} = require('./libs/addon.js')(config);
// var yolo = require('@vapi/node-yolo');
async function main(){
    console.log('Loading TensorFlow Coral...')
    await loadModel()
    console.log('Loaded TensorFlow Coral!')
    s.detectObject = async function(buffer,d,tx,frameLocation,callback){
        const timeStart = new Date()
        try{
            var isObjectDetectionSeparate = d.mon.detector_use_detect_object === '1'
            var width = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_y_object ? d.mon.detector_scale_y_object : d.mon.detector_scale_y)
            var height = parseFloat(isObjectDetectionSeparate  && d.mon.detector_scale_x_object ? d.mon.detector_scale_x_object : d.mon.detector_scale_x)
            const matrices = await detect(buffer, 0.7);
            if(matrices.length > 0){
                tx({
                    f: 'trigger',
                    id: d.id,
                    ke: d.ke,
                    details: {
                        plug: config.plug,
                        name: 'coral',
                        reason: 'object',
                        matrices: matrices,
                        imgHeight: width,
                        imgWidth: height,
                        time: (new Date()) - timeStart
                    },
                    frame: buffer
                })
            }
        }catch(error){
            console.error(`await detect`,error);
        }
        callback()
    }
}
main()
