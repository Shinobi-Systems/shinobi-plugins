{
  "targets": [
    {
      "target_name": "coraldetector",
      "sources": [
        "src/coral_detector.cc"
      ],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "tensorflow",
        "/usr/local/include",
        "<!(node -p \"process.arch === 'x64' ? '/usr/lib/x86_64-linux-gnu' : '/usr/lib/aarch64-linux-gnu'\")",
        "/usr/local/include/opencv4",
        "/usr/include/opencv4"
      ],
      "cflags": [
        "-std=c++11",
        "-O3",
        "-fPIC",
        "-frtti"
      ],
      "cflags_cc": [
        "-std=c++11",
        "-O3",
        "-fPIC",
        "-frtti"
      ],
      "ldflags": [
        "-L/usr/include",
        "<!(node -p \"process.arch === 'x64' ? '-L/usr/lib/x86_64-linux-gnu' : '-L/usr/lib/aarch64-linux-gnu'\")",
        "-ledgetpu"
      ],
      "libraries": [
        "/usr/local/lib/libtensorflow-lite.a",
        "<!(node -p \"process.arch === 'x64' ? '/usr/lib/x86_64-linux-gnu/libedgetpu.so' : '/usr/lib/aarch64-linux-gnu/libedgetpu.so'\")",
        "/usr/local/lib/libflatbuffers.a",
        "-lstdc++",
        "-ledgetpu",
        "-lopencv_core",
        "-lopencv_imgproc",
        "-lopencv_imgcodecs",
        "-lopencv_highgui",
        "-lopencv_dnn"
      ],
      "defines": [
        "NAPI_DISABLE_CPP_EXCEPTIONS"
      ],
      "xcode_settings": {
        "GCC_ENABLE_CPP_EXCEPTIONS": "YES"
      },
      "conditions": [
        ["OS=='linux'", {
          "cflags!": ["-fno-exceptions"],
          "cflags_cc!": ["-fno-exceptions"]
        }]
      ]
    }
  ]
}
