#include <napi.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <memory>
#include "tensorflow/lite/interpreter.h"
#include "tensorflow/lite/model.h"
#include "tensorflow/lite/kernels/register.h"
#include "edgetpu.h"
#include <cstring>
#include <iostream>
#include <opencv2/opencv.hpp>

// Global variables for model, interpreter, and EdgeTPU context
std::unique_ptr<tflite::Interpreter> interpreter;
std::unique_ptr<tflite::FlatBufferModel> model;
std::shared_ptr<edgetpu::EdgeTpuContext> tpu_context;

// Global class labels
std::vector<std::string> classLabels;

// Function to build EdgeTPU interpreter
std::unique_ptr<tflite::Interpreter> BuildEdgeTpuInterpreter(const tflite::FlatBufferModel &model, edgetpu::EdgeTpuContext *tpu_context) {
    tflite::ops::builtin::BuiltinOpResolver resolver;
    resolver.AddCustom(edgetpu::kCustomOp, edgetpu::RegisterCustomOp());

    std::unique_ptr<tflite::Interpreter> interpreter;
    if (tflite::InterpreterBuilder(model, resolver)(&interpreter) != kTfLiteOk) {
        std::cerr << "Failed to build interpreter." << std::endl;
        return nullptr;
    }

    interpreter->SetExternalContext(kTfLiteEdgeTpuContext, tpu_context);
    interpreter->SetNumThreads(1);
    if (interpreter->AllocateTensors() != kTfLiteOk) {
        std::cerr << "Failed to allocate tensors." << std::endl;
        return nullptr;
    }

    return interpreter;
}

// Function to load the model and EdgeTPU context
void LoadModel(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();

    if (info.Length() != 2) {
        Napi::TypeError::New(env, "2 arguments expected: (modelPath, labelsPath)").ThrowAsJavaScriptException();
        return;
    }

    std::string modelPath = info[0].As<Napi::String>().Utf8Value();
    std::string labelsPath = info[1].As<Napi::String>().Utf8Value();

    // Load the model
    model = tflite::FlatBufferModel::BuildFromFile(modelPath.c_str());
    if (!model) {
        Napi::Error::New(env, "Failed to load model").ThrowAsJavaScriptException();
        return;
    }

    // Initialize the EdgeTPU context
    tpu_context = edgetpu::EdgeTpuManager::GetSingleton()->OpenDevice();
    if (!tpu_context) {
        Napi::Error::New(env, "Failed to initialize EdgeTPU").ThrowAsJavaScriptException();
        return;
    }

    // Build the interpreter using the uploaded file's method
    interpreter = BuildEdgeTpuInterpreter(*model, tpu_context.get());
    if (!interpreter) {
        Napi::Error::New(env, "Failed to build interpreter").ThrowAsJavaScriptException();
        return;
    }

    // Load the class labels
    std::ifstream labelFile(labelsPath);
    std::string line;
    while (std::getline(labelFile, line)) {
        classLabels.push_back(line);
    }
    labelFile.close();
}

// Function to detect objects using the Coral TPU
Napi::Array DetectObjects(const Napi::CallbackInfo& info) {
    Napi::Env env = info.Env();

    if (info.Length() != 2) {
        Napi::TypeError::New(env, "2 arguments expected: (imageBuffer, threshold)").ThrowAsJavaScriptException();
        return Napi::Array::New(env);
    }

    Napi::Buffer<uint8_t> imageBuffer = info[0].As<Napi::Buffer<uint8_t>>();
    float threshold = info[1].As<Napi::Number>().FloatValue();

    // Decode the image buffer to a cv::Mat
    std::vector<uint8_t> bufferData(imageBuffer.Data(), imageBuffer.Data() + imageBuffer.Length());
    cv::Mat img = cv::imdecode(bufferData, cv::IMREAD_COLOR);

    // Prepare input tensor
    TfLiteTensor* input_tensor = interpreter->tensor(interpreter->inputs()[0]);
    const int height = input_tensor->dims->data[1];
    const int width = input_tensor->dims->data[2];
    const int channels = input_tensor->dims->data[3];
    const int row_elems = width * channels;

    // Resize the image to the model's input size
    cv::Mat resized;
    cv::resize(img, resized, cv::Size(width, height));

    uint8_t* dst = input_tensor->data.uint8;
    for (int row = 0; row < height; row++) {
        memcpy(dst, resized.ptr(row), row_elems);
        dst += row_elems;
    }

    // Run inference
    interpreter->Invoke();

    // Extract detection results
    TfLiteTensor* output_locations = interpreter->tensor(interpreter->outputs()[0]);
    TfLiteTensor* output_classes = interpreter->tensor(interpreter->outputs()[1]);
    TfLiteTensor* output_scores = interpreter->tensor(interpreter->outputs()[2]);
    TfLiteTensor* num_detections_ = interpreter->tensor(interpreter->outputs()[3]);

    const float* detection_locations = output_locations->data.f;
    const float* detection_classes = output_classes->data.f;
    const float* detection_scores = output_scores->data.f;
    const int num_detections = static_cast<int>(num_detections_->data.f[0]);

    // Create an array to store detections
    Napi::Array detections = Napi::Array::New(env);
    int index = 0;

    // Iterate through detections and filter based on threshold
    for (int i = 0; i < num_detections; i++) {
        float score = detection_scores[i];
        if (score < threshold) {
            continue;
        }

        std::string label = classLabels[static_cast<int>(detection_classes[i])];

        int ymin = static_cast<int>(detection_locations[4 * i + 0] * img.rows);
        int xmin = static_cast<int>(detection_locations[4 * i + 1] * img.cols);
        int ymax = static_cast<int>(detection_locations[4 * i + 2] * img.rows);
        int xmax = static_cast<int>(detection_locations[4 * i + 3] * img.cols);

        int width = xmax - xmin;
        int height = ymax - ymin;

        // Create a JavaScript object for each detection
        Napi::Object detection = Napi::Object::New(env);
        detection.Set("x", xmin);
        detection.Set("y", ymin);
        detection.Set("width", width);
        detection.Set("height", height);
        detection.Set("tag", label);
        detection.Set("confidence", score);

        detections.Set(index++, detection);
    }

    return detections;
}

// Function to unload the model and free resources
void UnloadModel(const Napi::CallbackInfo& info) {
    model.reset();
    interpreter.reset();
    classLabels.clear();
    tpu_context.reset();
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
    exports.Set(Napi::String::New(env, "loadModel"), Napi::Function::New(env, LoadModel));
    exports.Set(Napi::String::New(env, "detectObjects"), Napi::Function::New(env, DetectObjects));
    exports.Set(Napi::String::New(env, "unloadModel"), Napi::Function::New(env, UnloadModel));
    return exports;
}

NODE_API_MODULE(CoralDetector, Init)
