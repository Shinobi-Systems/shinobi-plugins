const coralDetector = require('../build/Release/coraldetector');

module.exports = (config) => {
    const modelPath = config?.modelPath || `${__dirname}/../models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite`;
    const labelsPath = config?.labelsPath || `${__dirname}/../models/coco_labels.txt`;

    function detect(imageBuffer, detectionThreshold = 0.6) {
        try {
            const detections = coralDetector.detectObjects(imageBuffer, detectionThreshold);
            return detections;
        } catch (error) {
            console.error('Error during detection:', error);
            throw error;
        }
    }

    async function loadNet() {
        try {
            await coralDetector.loadModel(modelPath, labelsPath);
            console.log('Model loaded successfully');
        } catch (error) {
            console.error('Error loading model:', error);
            throw error;
        }
    }

    function unloadNet() {
        try {
            coralDetector.unloadModel();
            console.log('Model unloaded');
        } catch (error) {
            console.error('Error unloading model:', error);
            throw error;
        }
    }

    return {
        detect,
        loadModel: loadNet,
        unloadModel: unloadNet,
        coralDetector,
    };
};
