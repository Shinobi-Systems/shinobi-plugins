# Google Coral Edge TPU N-API Module for Shinobi

> **IMPORTANT NOTICE** : We recommend installing **Ubuntu 22.04.5** but 24.04 shall work as well.

> Ubuntu 22.04 ISO Download : https://releases.ubuntu.com/jammy/ubuntu-22.04.5-live-server-amd64.iso
> Ubuntu 24.04 ISO Download : https://releases.ubuntu.com/noble/ubuntu-24.04.1-live-server-amd64.iso

This is a C++ based module for Coral Edge TPU Object Detection and a plugin for Shinobi (https://shinobi.video).

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **Plugin Manager** tab.
3. In the listing select the latest **"Coral TPU"** plugin to Download.
4. Hit `Run Installer` for the newly downloaded plugin.
    - This plugin's installer requires a system reboot after installing Coral Edge TPU Drivers. *Coral Edge TPU Drivers must be installed prior to running the installer*.
5. Reopen the Plugin Manager and run the `Test Object Detector` command.
6. Once Installed click "Enable" and restart Shinobi.
7. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)

Your test should look similar to this.

```
Frame 176 detected in 0.278s
[
  {
    class_id: 0,
    confidence: 0.6456838250160217,
    box: { x: 254, y: 432, width: 58, height: 48 },
    tag: 'person'
  }
]
Frame 177 detected in 0.276s
[
  {
    class_id: 0,
    confidence: 0.6623225212097168,
    box: { x: 256, y: 433, width: 54, height: 52 },
    tag: 'person'
  }
]
Test done in 47.241s!
#END_PROCESS
```
