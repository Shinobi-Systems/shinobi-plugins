#!/bin/bash

DIR=`dirname $0`

sed -i 's/\r$//' $DIR/INSTALL-python.sh
sed -i 's/\r$//' $DIR/INSTALL-cmake-316.sh
sed -i 's/\r$//' $DIR/INSTALL-flatbuffer.sh
sed -i 's/\r$//' $DIR/INSTALL-tfa.sh
sed -i 's/\r$//' $DIR/INSTALL-opencv.sh
sed -i 's/\r$//' $DIR/INSTALL-coral.sh

chmod +x $DIR/INSTALL-python.sh
chmod +x $DIR/INSTALL-cmake-316.sh
chmod +x $DIR/INSTALL-flatbuffer.sh
chmod +x $DIR/INSTALL-tfa.sh
chmod +x $DIR/INSTALL-opencv.sh
chmod +x $DIR/INSTALL-coral.sh
bash $DIR/INSTALL-python.sh
echo "========================="
echo "CMake 3.30.3 Installation"
bash $DIR/INSTALL-cmake-316.sh
echo "DONE! CMake 3.30.3 Installation"
echo "========================="
echo "Flatbuffer Installation"
bash $DIR/INSTALL-flatbuffer.sh
echo "DONE! Flatbuffer Installation"
echo "========================="
echo "TensorFlow Installation"
bash $DIR/INSTALL-tfa.sh
echo "DONE! TensorFlow Installation"
echo "========================="
echo "OpenCV Installation"
bash $DIR/INSTALL-opencv.sh
echo "DONE! OpenCV Installation"
echo "========================="
echo "Coral TPU Driver Installation"
bash $DIR/INSTALL-coral.sh
echo "DONE! Coral TPU Driver Installation"
echo "========================="

cd $DIR/../
