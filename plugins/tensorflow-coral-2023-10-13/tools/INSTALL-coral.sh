#!/bin/bash

ARCH=$(uname -m)
DIR=$(dirname "$0")
cd "$DIR/../"

if [ ! -d "libcoral" ]; then
    echo "Cloning libcoral repository..."
    git clone https://github.com/google-coral/libcoral
else
    echo "libcoral directory already exists. Skipping clone."
fi

# Get Ubuntu version
UBUNTU_VERSION=$(lsb_release -rs 2>/dev/null)

# Check if /dev/apex_0 exists
if [ ! -e /dev/apex_0 ]; then
    echo "/dev/apex_0 not found."
    echo "Installing Coral TPU Drivers..."
    if [ -x "$(command -v apt)" ]; then

        # Add the repository for Coral TPU
        echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list
        curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

        # Update the package list and install the required libraries
        sudo apt-get update -y
        sudo apt-get install dkms -y

        # Check if the system is Ubuntu 24.04
        if [[ "$UBUNTU_VERSION" == "24.04" ]]; then
            echo "Detected Ubuntu 24.04. Installing specific gasket-dkms .deb package..."
            wget https://cdn.shinobi.video/installers/gasket-dkms_1.0-18_all.deb
            sudo dpkg -i gasket-dkms_1.0-18_all.deb

            # Fix any broken dependencies
            sudo apt-get install -f -y
        else
            echo "Installing gasket-dkms via apt..."
            sudo apt-get install gasket-dkms -y
        fi


        sudo apt-get install libedgetpu1-std -y
        sudo apt-get install libedgetpu-dev -y
        # sudo apt-get install python3-tflite-runtime -y

        # Udev rules for the Edge TPU
        sudo sh -c "echo 'SUBSYSTEM==\"apex\", MODE=\"0660\", GROUP=\"apex\"' >> /etc/udev/rules.d/65-apex.rules"
        sudo groupadd apex
        sudo adduser "$USER" apex

        # sudo apt-get install python3-pycoral -y
        sudo usermod -aG plugdev $USER
        echo "------------------------------"
        echo "Reboot is required. Do it now?"
        echo "------------------------------"
        echo "(y)es or (N)o. Default is No."
        read rebootTheMachineHomie

        if [ "$rebootTheMachineHomie" = "y" ] || [ "$rebootTheMachineHomie" = "Y" ]; then
            echo "Rebooting..."
            echo "IMPORTANT! : Be sure to run this installer again after reboot!!!"
            sudo reboot
            exit
        fi
    else
        echo "This script is only supported on Debian-based systems. Try Ubuntu 22.04.3 or similar."
    fi
else
    echo "/dev/apex_0 found. Coral TPU drivers are already installed."
fi
