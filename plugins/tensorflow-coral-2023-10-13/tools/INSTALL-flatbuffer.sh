#!/bin/sh
DIR=$(dirname "$0")
cd "$DIR/../"

# FlatBuffers version or commit hash you want to use
FLATBUFFERS_VERSION="v2.0.0"  # Replace with the version or commit you want

# Check if flatbuffers directory already exists
if [ ! -d "flatbuffers" ]; then
    echo "Cloning FlatBuffers repository..."
    git clone https://github.com/google/flatbuffers.git
    cd flatbuffers
    echo "Checking out FlatBuffers version: $FLATBUFFERS_VERSION"
    git checkout tags/$FLATBUFFERS_VERSION
else
    echo "FlatBuffers directory already exists, skipping clone..."
    cd flatbuffers
    # Check if the correct version is already checked out
    CURRENT_VERSION=$(git describe --tags --exact-match 2>/dev/null || echo "none")
    if [ "$CURRENT_VERSION" != "$FLATBUFFERS_VERSION" ]; then
        echo "Checking out FlatBuffers version: $FLATBUFFERS_VERSION"
        git fetch --all --tags
        git checkout tags/$FLATBUFFERS_VERSION
    else
        echo "FlatBuffers is already at the desired version: $FLATBUFFERS_VERSION"
    fi
fi

# Detect architecture
ARCH=$(uname -m)

# Check if the compiler supports -Wno-error=class-memaccess silently
echo 'int main(){}' > test.cpp
CXX_FLAGS=""
if g++ -Wno-error=class-memaccess -x c++ -c -o /dev/null test.cpp >/dev/null 2>&1; then
    CXX_FLAGS="-Wno-error=class-memaccess"
fi
rm test.cpp

# Build FlatBuffers
cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS="-fPIC $CXX_FLAGS" -DCMAKE_POSITION_INDEPENDENT_CODE=ON .
make -j$(nproc)
sudo make install
