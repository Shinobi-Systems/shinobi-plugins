#!/bin/bash

# Get the current CMake version
CURRENT_VERSION=$(cmake --version 2>/dev/null | head -n1 | awk '{print $3}')

# Get system architecture
ARCH=$(uname -m)

# Set the desired CMake version
DESIRED_VERSION="3.30.3"

# Function to download and install CMake based on architecture
install_cmake() {
    local file_name

    if [ "$ARCH" == "aarch64" ]; then
        file_name="cmake-${DESIRED_VERSION}-linux-aarch64.sh"
        echo "Installing CMake for ARM (aarch64)..."
    elif [ "$ARCH" == "x86_64" ]; then
        file_name="cmake-${DESIRED_VERSION}-linux-x86_64.sh"
        echo "Installing CMake for x86_64..."
    else
        echo "Unsupported architecture: $ARCH"
        exit 1
    fi

    # Check if the file already exists
    if [ ! -f "$file_name" ]; then
        echo "Downloading CMake version $DESIRED_VERSION..."
        wget https://github.com/Kitware/CMake/releases/download/v${DESIRED_VERSION}/$file_name -O $file_name
        chmod +x ./$file_name
    else
        echo "CMake installer $file_name already exists. Skipping download."
    fi

    # Install CMake
    sudo ./$file_name --skip-license --prefix=/usr/local
}

# Compare CMake versions using sort -V for proper version comparison
if [ -z "$CURRENT_VERSION" ] || [ "$(printf '%s\n' "$CURRENT_VERSION" "$DESIRED_VERSION" | sort -V | head -n1)" != "$DESIRED_VERSION" ]; then
    echo "CMake version is less than $DESIRED_VERSION or not installed. Installing CMake $DESIRED_VERSION..."
    install_cmake
else
    echo "CMake version is $CURRENT_VERSION, which is $DESIRED_VERSION or higher. No installation required."
fi

# Verify the installation
echo "====== CMake Current Version ======"
cmake --version
echo "==================================="
