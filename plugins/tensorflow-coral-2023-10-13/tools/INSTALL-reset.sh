#!/bin/bash
DIR=$(dirname "$0")
cd "$DIR/.."

rm /usr/local/lib/libtensorflow-lite.a
rm /usr/local/lib/libflatbuffers.a
rm -rf tensorflow/tflite_build
