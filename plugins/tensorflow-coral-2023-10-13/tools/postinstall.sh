DIR=`dirname $0`
sed -i 's/\r$//' $DIR/DOWNLOAD-models.sh
chmod +x $DIR/DOWNLOAD-models.sh
bash $DIR/DOWNLOAD-models.sh

npm run build
if [ ! -e "./conf.json" ]; then
    echo "Creating conf.json"
    sudo cp conf.sample.json conf.json
    currentFolderName=${PWD##*/}
	echo "Adding Random Plugin Key to Main Configuration"
	node $DIR/../../../tools/modifyConfigurationForPlugin.js $currentFolderName key=$(head -c 64 < /dev/urandom | sha256sum | awk '{print substr($1,1,60)}') tfjsBuild=$tfjsBuildVal
else
    echo "conf.json already exists..."
fi
