const coralDetector = require('../build/Release/coraldetector');
const { spawn } = require('child_process');

// Paths to the model and labels
const modelPath = `${__dirname}/../models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite`;
const labelsPath = `${__dirname}/../models/coco_labels.txt`;

// Get the RTSP URL from the first parameter
const rtspUrl = process.argv[2];
const mjpegTest = process.argv[3] === 'mjpeg';
if (!rtspUrl) {
    console.error('Please provide an RTSP URL as the first parameter.');
    process.exit(1);
}

// Load the model
console.log('modelPath:', modelPath);
console.log('labelsPath:', labelsPath);
coralDetector.loadModel(modelPath, labelsPath);
console.log('Model loaded successfully');

// FFmpeg command and options for reading RTSP stream and outputting raw RGB frames
const ffmpegArgs = mjpegTest ? [
    '-re',
    '-i', rtspUrl,           // Input RTSP stream
    '-vf', 'scale=300:300',
    '-f', 'mjpeg',      // Output as a pipe (raw image frames)
    '-an',                   // Disable audio
    '-'                      // Output to stdout (pipe)
] : [
    '-re',
    '-i', rtspUrl,           // Input RTSP stream
    '-vf', 'scale=300:300',
    '-pix_fmt', 'rgb24',     // Convert frames to RGB
    '-f', 'image2pipe',      // Output as a pipe (raw image frames)
    '-vcodec', 'rawvideo',   // Output raw video format
    '-an',                   // Disable audio
    '-'                      // Output to stdout (pipe)
];

// Spawn FFmpeg process
const ffmpeg = spawn('ffmpeg', ffmpegArgs);
// Each frame will be 300x300 RGB image (300x300x3 = 270000 bytes per frame)
const frameSize = 300 * 300 * 3;
let frameBuffer = Buffer.alloc(0); // Buffer to accumulate data
let fpsCount = 0;
let currentFps = 0;

// Calculate and log FPS every second
setInterval(function(){
    currentFps = fpsCount;
    console.log(`FPS Detected: ${currentFps}`);
    fpsCount = 0;
}, 1000);

// Event listener for data (raw frames) from FFmpeg
ffmpeg.stdout.on('data', (data) => {
    // Accumulate the incoming data into the buffer
    frameBuffer = Buffer.concat([frameBuffer, data]);

    // Process all complete frames in the buffer
    while (frameBuffer.length >= frameSize) {
        const startTime = Date.now(); // Start time for inference

        // Extract a full frame from the buffer
        const completeFrame = frameBuffer.slice(0, frameSize);

        // Remove the processed frame from the buffer
        frameBuffer = frameBuffer.slice(frameSize);

        const detectionThreshold = 0.03; // Detection confidence threshold

        // Call the Coral Edge TPU detector with the frame buffer
        const detections = coralDetector.detectObjects(completeFrame, detectionThreshold);

        // Calculate the inference time
        const detectTime = Date.now() - startTime;
        fpsCount++; // Increment FPS count

        // Log the inference time and current FPS
        console.log(`${detectTime}ms inference, ${currentFps} FPS, Detections: ${detections.length}`);
    }
});

// Event listener for errors from the FFmpeg process
ffmpeg.stderr.on('data', (data) => {
    console.log(`FFmpeg stderr: ${data}`);
});

// Event listener for when FFmpeg process finishes
ffmpeg.on('close', (code) => {
    console.log(`FFmpeg process exited with code ${code}`);

    // Unload the model when FFmpeg process ends
    coralDetector.unloadModel();
    console.log('Model unloaded');
});

// Event listener for FFmpeg errors
ffmpeg.on('error', (error) => {
    console.error('Error spawning FFmpeg process:', error);
});

// Event listener for process exit
process.on('exit', () => {
    coralDetector.unloadModel();
    console.log('Model unloaded due to process exit');
});
