#!/bin/bash
UBUNTU_VERSION=$(lsb_release -rs 2>/dev/null)
if [[ "$UBUNTU_VERSION" == "24.04" ]]; then
    echo "Checking for Python 3.10 installation..."

    # Check if Python 3.10 is already installed
    if python3.10 --version > /dev/null 2>&1; then
      echo "Python 3.10 is already installed."
    else
      echo "Python 3.10 is not installed. Installing it now..."

      # Add deadsnakes PPA and update package list (auto-confirm addition)
      sudo add-apt-repository ppa:deadsnakes/ppa -y > /dev/null 2>&1
      sudo apt update -y

      # Install Python 3.10 and necessary packages
      sudo apt install -y python3.10 python3.10-venv python3.10-dev python3.10-distutils

      # Verify installation
      if python3.10 --version > /dev/null 2>&1; then
        echo "Python 3.10 installed successfully."
      else
        echo "Failed to install Python 3.10. Exiting..."
        exit 1
      fi
    fi

    # Set Python 3.10 as the default (without interaction)
    echo "Setting Python 3.10 as the default Python3 version..."
    sudo update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.10 1 > /dev/null 2>&1
    sudo update-alternatives --set python3 /usr/bin/python3.10 > /dev/null 2>&1

    # Install pip for Python 3.10
    if ! python3.10 -m pip --version > /dev/null 2>&1; then
      echo "Installing pip for Python 3.10..."
      curl -s https://bootstrap.pypa.io/get-pip.py -o get-pip.py
      sudo python3.10 get-pip.py > /dev/null 2>&1
      echo "Pip installed for Python 3.10."
    else
      echo "Pip is already installed for Python 3.10."
    fi

    # Clean up pip installation file
    rm -f get-pip.py

    # Verify default Python and pip versions
    echo "Verifying default Python and pip versions..."
    python3 --version
    pip3 --version

    echo "Python 3.10 setup is complete."
fi
