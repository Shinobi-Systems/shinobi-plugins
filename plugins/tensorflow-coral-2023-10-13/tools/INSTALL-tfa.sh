#!/bin/bash
DIR=$(dirname "$0")
# Get the Ubuntu version
UBUNTU_VERSION=$(lsb_release -rs 2>/dev/null)
# TensorFlow version tag
TENSORFLOW_VERSION="v2.5.0"
if [[ "$UBUNTU_VERSION" == "24.04" ]]; then
    TENSORFLOW_VERSION="v2.6.0"
fi
TENSORFLOW_DIR="tensorflow"

# Check architecture
ARCH=$(uname -m)

# Set TensorFlow Lite library path based on architecture
if [ "$ARCH" == "x86_64" ]; then
    TFLITE_LIB="/usr/local/lib/libtensorflow-lite.a"
    BUILD_DIR="linux_x86_64"
elif [ "$ARCH" == "aarch64" ]; then
    TFLITE_LIB="/usr/local/lib/libtensorflow-lite.a"
    BUILD_DIR="linux_aarch64"
else
    echo "Unsupported architecture: $ARCH"
    exit 1
fi

# Check if TensorFlow Lite library already exists
if [ -f "$TFLITE_LIB" ]; then
    echo "TensorFlow Lite library already exists at $TFLITE_LIB. Skipping build."
    exit 0
fi

# Clone TensorFlow repository if not already cloned or if the version is different
if [ ! -d "$TENSORFLOW_DIR" ]; then
    echo "Cloning TensorFlow version $TENSORFLOW_VERSION..."
    git clone --branch $TENSORFLOW_VERSION https://github.com/tensorflow/tensorflow.git "$TENSORFLOW_DIR"
else
    cd "$TENSORFLOW_DIR" || { echo "Failed to enter TensorFlow directory"; exit 1; }
    CURRENT_VERSION=$(git describe --tags --exact-match 2>/dev/null || echo "none")
    if [ "$CURRENT_VERSION" != "$TENSORFLOW_VERSION" ]; then
        echo "Existing TensorFlow directory is not at version $TENSORFLOW_VERSION. Checking out the correct version..."
        git fetch --tags
        git checkout $TENSORFLOW_VERSION
    else
        echo "TensorFlow is already at version $TENSORFLOW_VERSION, skipping cloning..."
    fi
    cd ..
fi



# Common setup
sudo apt-get update -y
sudo apt-get install zlib1g-dev -y

# Check for Ubuntu 24.04-specific setup
if [[ "$UBUNTU_VERSION" == "24.04" ]]; then
    # Ubuntu 24.04 specific setup
    sudo apt-get install -y gcc-9 g++-9
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90
    sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 90
fi

# Navigate into the TensorFlow directory
cd "$TENSORFLOW_DIR" || { echo "Failed to enter TensorFlow directory"; exit 1; }

# Build TensorFlow Lite using the Makefile
echo "Building TensorFlow Lite..."
$DIR/../tensorflow/lite/tools/make/download_dependencies.sh
sed -i '1i#include <limits>' tensorflow/lite/tools/make/downloads/ruy/ruy/block_map.cc

# Check for Ubuntu version-specific build commands
if [[ "$UBUNTU_VERSION" == "24.04" ]]; then
    # Ubuntu 24.04 specific build
    $DIR/../tensorflow/lite/tools/make/build_lib.sh V=1 || { echo "TensorFlow Lite build failed"; exit 1; }
else
    # Normal build for other versions
    $DIR/../tensorflow/lite/tools/make/build_lib.sh || { echo "TensorFlow Lite build failed"; exit 1; }
fi

# Copy the built library to the system directory
echo "Copying TensorFlow Lite libraries to system directories..."
sudo cp "tensorflow/lite/tools/make/gen/$BUILD_DIR/lib/libtensorflow-lite.a" /usr/local/lib/ || { echo "Failed to copy libtensorflow-lite.a"; exit 1; }
sudo ldconfig

echo "TensorFlow Lite build complete!"

# Optionally clean up the cloned TensorFlow repository
CLEANUP="${1:-n}"

if [[ "$CLEANUP" == "y" || "$CLEANUP" == "Y" ]]; then
    echo "Cleaning up TensorFlow repository..."
    cd ..
    rm -rf "$TENSORFLOW_DIR"
    echo "Cleanup complete!"
else
    echo "Skipping cleanup."
fi
