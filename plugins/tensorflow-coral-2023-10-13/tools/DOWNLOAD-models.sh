#!/bin/bash
DIR="$(dirname "$0")"
cd "$DIR/../"
echo "Getting coral object detection models..."

mkdir -p models

MODEL_FILE="models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite"
LABELS_FILE="models/coco_labels.txt"

# Check if the model file exists
if [ ! -f "$MODEL_FILE" ]; then
    echo "Model file not found, downloading..."
    wget -O "$MODEL_FILE" "https://cdn.shinobi.video/binaries/tensorflow/coral/models-2021-01-26/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite"
else
    echo "Model file already exists, skipping download."
fi

# Check if the labels file exists
if [ ! -f "$LABELS_FILE" ]; then
    echo "Labels file not found, downloading..."
    wget -O "$LABELS_FILE" "https://cdn.shinobi.video/binaries/tensorflow/coral/models-2021-01-26/plugins_tensorflow-coral_models_coco_labels.txt"
else
    echo "Labels file already exists, skipping download."
fi

echo "Models downloaded."

echo "!!!IMPORTANT!!!"
echo "Run the Test Command to see if the installation was successful!"
