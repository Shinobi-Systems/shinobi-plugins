const coralDetector = require('../build/Release/coraldetector');
const fetch = require('node-fetch');
const imageSize = require('image-size'); // You will need to install this package: npm install image-size

// Paths to the model and labels
const modelPath = `${__dirname}/../models/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite`;
const labelsPath = `${__dirname}/../models/coco_labels.txt`;

// List of image URLs to test
const imageUrls = [
    'https://cdn.shinobi.video/images/test/poses.jpg',
    'https://cdn.shinobi.video/images/test/car.jpg',
    'https://cdn.shinobi.video/images/test/bear.jpg',
    'https://cdn.shinobi.video/images/test/stealing/1.png',
    'https://cdn.shinobi.video/images/test/people.jpg'
];

// Load the model
console.log('modelPath:', modelPath);
console.log('labelsPath:', labelsPath);
coralDetector.loadModel(modelPath, labelsPath);
console.log('Model loaded successfully');

// Function to process an image and detect objects
async function processImage(imageUrl) {
  try {
    const response = await fetch(imageUrl);
    const arrayBuffer = await response.arrayBuffer();
    const imageBuffer = Buffer.from(arrayBuffer); // Convert to Buffer

    // Get image dimensions using image-size
    const { width, height } = imageSize(imageBuffer);

    const detectionThreshold = 0.03;

    // Call the detector with the image buffer, threshold, and image dimensions
    const detections = coralDetector.detectObjects(imageBuffer, detectionThreshold, width, height);

    // Print the detections
    console.log(`Detections for ${imageUrl}:`, detections);
  } catch (error) {
    console.error(`Error fetching or processing image ${imageUrl}:`, error);
  }
}

// Function to process all images sequentially
(async () => {
  for (const imageUrl of imageUrls) {
    await processImage(imageUrl);
  }

  // Unload the model
  coralDetector.unloadModel();
  console.log('Model unloaded');
})();
