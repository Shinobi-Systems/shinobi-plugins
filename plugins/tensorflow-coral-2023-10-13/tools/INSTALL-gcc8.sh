
# GCC Version
GCC_VERSION="8.3.0"

# Download GCC Source
echo "Downloading GCC source code..."
cd /tmp
wget https://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz

# Extract Archive
echo "Extracting archive..."
tar -xf gcc-$GCC_VERSION.tar.gz

# Create Build Directory
echo "Creating build directory..."
cd gcc-$GCC_VERSION
mkdir build
cd build

# Configure
echo "Configuring..."
../configure --enable-languages=c,c++ --disable-multilib

# Make
echo "Compiling (this may take a while)..."
make -j$(nproc)

# Install
echo "Installing..."
make install

# Verify Installation
echo "Verifying installation..."
gcc --version

echo "GCC $GCC_VERSION has been installed successfully!"
