# Code Project AI

Proxy layer between Shinobi Video and Code Project AI Server

## Features

One stop shop plugin for all CodeProject AI models.

Automatically send frames to the relevant model based on the configuration on the CodeProject AI server.

Interact with Face Manager to register and unregister faces.

Event message special properties:

- person - When face is being identify, will indicate the face name from face manager (on top of the tag that represents the image)
- duration - time took the sever to process the request (including upload file)

## Configuration

```bash
nano conf.json
```

Here is a sample of a Host configuration for the plugin

```jsonc
{
  "plug": "CodeProject-AI", 
  "host": "localhost",
  "tfjsBuild": "cpu",
  "port": 8080,
  "hostPort": 58088,
  "key": "CodeProject-AI",
  "mode": "client",
  "type": "detector",
  "api": {
     "host": "HOSTNAME",    // Hostname or IP to access Code Project AI Server
     "port": 32168,         // Port to access Code Project AI Server
     "isSSL": false,        // Is CodeProejct API works with HTTPS
     "minConfidence": 0.7,  // Minimum confidence level, defaults to 0.4 (40%)
     "apiKey": "API_KEY"    // Set the API Key in case defined in CodeProject AI Server, if not being used - remove that property
  },
  "enabled": true,
  "eventHandlers": {
     "motionDetection": [   // Models to process motion detection, not stating models or null value will prevent processing
        "ObjectDetectionYolo"
     ],
     "objectDetection": [   // Models to process object detection, not stating models or null value will prevent processing
        "FaceProcessing"
     ]
  }
}
```
