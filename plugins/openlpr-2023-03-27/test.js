// Base Init >>
var fs = require('fs').promises;
var config = require('./conf.json');
// Base Init />>
const openLPR = require('node-openlpr')

const {
    detectPlate,
} = openLPR(config.apiHost || 'http://localhost:5000');


async function runDetection(buffer){
    var timeStart = new Date()
    try{
        const detections = await detectPlate(buffer)
        matrices = []
        detections.forEach(function(v){
            matrices.push({
              x:v.box.x,
              y:v.box.y,
              width:v.box.w,
              height:v.box.h,
              tag:v.className,
              confidence:v.probability,
            })
        })
        return matrices
    }catch(error){
        console.error(`await detector.detect`,error);
    }
    return []
}

const testImageUrl = `https://cdn.shinobi.video/images/test/licensePlate-Dubai.jpg`
const testImageUrl2 = `https://cdn.shinobi.video/images/test/licensePlate-US.jpg`
const testImageUrl3 = `https://cdn.shinobi.video/images/test/licensePlate-US-2.jpg`
const runTest = async (imageUrl) => {
    console.log(`Loading ${imageUrl}`)
    const response = await fetch(imageUrl);
    const frameBuffer = await response.buffer();
    console.log(`Detecting upon ${imageUrl}`)
    const results = await runDetection(frameBuffer)
    if(results[0]){
        var mats = []
        console.log('Detected Objects!')
        results.forEach(function(matrix){
            console.log(matrix)
        })
    }else{
        console.log('No Matrices...')
    }
    console.log(`Done ${imageUrl}`)
}
const allTests = async () => {
    await runTest(testImageUrl)
    await runTest(testImageUrl2)
    await runTest(testImageUrl3)
}
allTests()
