const fs = require('fs').promises;
const {
    loadClassNames,
    loadNet,
    detect
} = require('./libs/addon.js');
const main = async () => {
    const startTime = new Date().getTime()
    const classNames = await loadClassNames()
    await loadNet(false)
    console.log(classNames)
    const images = [
        '1.jpg',
        '2.jpg',
        '3.jpg',
        '4.jpg',
        '5.jpg',
        '6.jpg',
    ]
    for (let i = 0; i < images.length; i++) {
        const name = images[i]
        const filePath = `images/${name}`
        const imageBuffer = await fs.readFile(filePath);
        const detectResponse = await detect(imageBuffer,{
            inputWidth: 640.0,
            inputHeight: 640.0,
            scoreThreshold: 0.2,
            nmsThreshold: 0.4,
            confidenceThreshold: 0.4,
        })
        detectResponse.forEach((matrix) => {
            matrix.tag = classNames[matrix.class_id]
        })
        console.log(detectResponse)
        console.log()
    }
    const endTime = new Date().getTime()
    const inferenceTime = (endTime - startTime) / 1000
    console.log(`Test done in ${inferenceTime}s!`)
};

main().then(() => {
    process.exit()
}).catch(console.error);
