{
  "targets": [
    {
      "target_name": "addon",
      "sources": ["src/addon.cpp"],
      "cflags_cc": ["-fexceptions", "-frtti"],
      "include_dirs": [
        "<!@(node -p \"require('node-addon-api').include\")",
        "/usr/local/include/opencv4",
        "/usr/include/opencv4",
        "/usr/include/opencv4/opencv2"
      ],
      "libraries": [
        "-lopencv_core",
        "-lopencv_imgproc",
        "-lopencv_imgcodecs",
        "-lopencv_highgui",
        "-lopencv_dnn"
      ]
    }
  ]
}
