#!/bin/bash
DIR=$(dirname $0)
if [ -x "$(command -v nvidia-smi)" ]; then
    echo "nvidia-smi detected"
else
    echo "Install GPU Support for NVIDIA GPUs?"
    echo "NVIDIA Drivers, CUDA Toolkit and CUDNN will be installed."
    echo "WARNING! : You need to reboot after NVIDIA software is installed."
    echo "Which means saying Yes will mean you need to run this installer again after reboot."
    echo "(y)es or (N)o"
    read doGpuInstalled
    if [ "$doGpuInstalled" = "y" ] || [ "$doGpuInstalled" = "Y" ]; then
        sh $DIR/cuda.sh
    fi
fi

sh $DIR/install_gcc.sh
bash $DIR/install_opencv.sh
