#!/bin/sh

# Function to detect if we are on a Jetson Nano
is_jetson_nano() {
    jetson_info=$(tr -d '\0' < /proc/device-tree/model 2>/dev/null)

    case "$jetson_info" in
        *"Jetson Nano"*)
            return 0
            ;;
        *)
            return 1
            ;;
    esac
}

# Check if the system is a Jetson Nano
if is_jetson_nano; then
    echo "Jetson Nano detected. Running INSTALL-jetson.sh..."
    bash INSTALL-jetson.sh
else
    echo "Jetson Nano NOT detected. Running INSTALL-else.sh..."
    bash INSTALL-else.sh
fi
