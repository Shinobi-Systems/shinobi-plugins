#!/bin/bash

# Directory to store the weights
WEIGHTS_DIR="./weights"
mkdir -p $WEIGHTS_DIR

# URLs for YOLOv5 pre-trained weights
Yolov5sURL="https://github.com/ultralytics/yolov5/releases/download/v7.0/yolov5s.pt"
Yolov5mURL="https://github.com/ultralytics/yolov5/releases/download/v7.0/yolov5m.pt"
Yolov5lURL="https://github.com/ultralytics/yolov5/releases/download/v7.0/yolov5l.pt"
# ... add URLs for other models (yolov5l.pt, yolov5x.pt) if needed

# Function to download weight if not exists
download_weight() {
    local url=$1
    local file_path="$WEIGHTS_DIR/$(basename $url)"

    if [ ! -f "$file_path" ]; then
        echo "Downloading $(basename $url) ..."
        wget -O "$file_path" "$url"
    else
        echo "$(basename $url) already exists."
    fi
}

# Download the weights
download_weight $Yolov5sURL
download_weight $Yolov5mURL
download_weight $Yolov5lURL
if [ ! -f "$WEIGHTS_DIR/classes.txt" ]; then
    wget -O "$WEIGHTS_DIR/classes.txt" https://cdn.shinobi.video/weights/yolov5-objectdetection/config_files/classes.txt
fi
echo "Weights download completed."
