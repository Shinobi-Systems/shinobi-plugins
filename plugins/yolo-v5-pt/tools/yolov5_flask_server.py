import sys
import argparse
from flask import Flask, request, jsonify
import torch
from models.common import DetectMultiBackend
from utils.general import non_max_suppression, scale_boxes, xyxy2xywh, check_img_size
from utils.torch_utils import select_device, smart_inference_mode
from PIL import Image
import numpy as np
import logging
from werkzeug import serving

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--port', type=int, default=8989, help='port number')
parser.add_argument('--weights', type=str, default='weights/yolov5s.pt', help='model path')
parser.add_argument('--device', default='', help='cuda device, i.e. 0 or 0,1,2,3 or cpu')
parser.add_argument('--imgszw', type=int, default=640, help='inference width (pixels)')
parser.add_argument('--imgszh', type=int, default=480, help='inference height (pixels)')
parser.add_argument('--conf-thres', type=float, default=0.46, help='confidence threshold')
parser.add_argument('--iou-thres', type=float, default=0.4, help='NMS IoU threshold')
parser.add_argument('--classes', nargs='+', type=int, help='filter by class')
parser.add_argument('--agnostic-nms', action='store_true', help='class-agnostic NMS')
parser.add_argument('--max-det', type=int, default=1000, help='maximum detections per image')
parser.add_argument('--classes-file', type=str, default='weights/classes.txt', help='Path to classes.txt file')
parser.add_argument('--datayaml', type=str, default='data/coco128.yaml', help='dataset.yaml path')
args = parser.parse_args()

app = Flask(__name__)

# Load class names
with open(args.classes_file, 'r') as f:
    names = [line.strip() for line in f.readlines()]

# Load the model
device = select_device(args.device)
model = DetectMultiBackend(args.weights, device=device, dnn=False, data=args.datayaml, fp16=False)
stride, names, pt = model.stride, model.names, model.pt
imgsz = check_img_size((args.imgszw,args.imgszh), s=stride)


@app.route('/detect', methods=['POST'])
@smart_inference_mode()
def detect():
    if 'file' not in request.files:
        return jsonify({'error': 'No file part'}), 400

    camera_id = request.form.get('cameraId')
    file = request.files['file']
    if file.filename == '':
        return jsonify({'error': 'No selected file'}), 400
    image = Image.open(file.stream).convert('RGB')
    img = np.array(image)
    # Convert image to PyTorch tensor and permute dimensions to [C, H, W]
    im = torch.from_numpy(img).permute(2,0,1).to(model.device)  # Permute dimensions to [C, H, W]
    im = im.half() if model.fp16 else im.float()  # Convert image to half precision float if model uses FP16, else use FP32
    im /= 255.0  # Normalize image from 0 - 255 to 0.0 - 1.0
    if len(im.shape) == 3:
        im = im[None]  # Add a batch dimension
    if model.xml and im.shape[0] > 1:
        ims = torch.chunk(im, im.shape[0], 0)
    # Inference
    # if hasattr(model, 'xml') and model.xml and im.shape[0] > 1:  # This condition is model specific and might not be relevant for all YOLOv5 models
    #     pred = None
    #     ims = torch.chunk(im, im.shape[0], 0)  # Split the batch into individual images
    #     for image in ims:
    #         if pred is None:
    #             pred = model(image, augment=False, visualize=False).unsqueeze(0)
    #         else:
    #             pred = torch.cat((pred, model(image, augment=False, visualize=False).unsqueeze(0)), dim=0)
    #     pred = [pred, None]
    # else:
    if model.xml and im.shape[0] > 1:
        pred = None
        for image in ims:
            if pred is None:
                pred = model(image, augment=False, visualize=False).unsqueeze(0)
            else:
                pred = torch.cat((pred, model(image, augment=False, visualize=False).unsqueeze(0)), dim=0)
        pred = [pred, None]
    else:
        pred = model(im, augment=False, visualize=False)
    pred = non_max_suppression(pred, args.conf_thres, args.iou_thres, args.classes, args.agnostic_nms, args.max_det)


    formatted_predictions = []
    for det in pred:
        if det is not None and len(det):
            det = det.to('cpu')
            # det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], img.shape).round()
            for *xyxy, conf, cls in det:
                cls = int(cls)
                conf = float(conf)
                x1, y1, x2, y2 = map(int, xyxy)
                x = x1
                y = y1
                width = x2 - x1
                height = y2 - y1

                formatted_predictions.append({
                    'tag': names[cls],
                    'confidence': conf,
                    'x': x,
                    'y': y,
                    'width': width,
                    'height': height
                })

    return jsonify(formatted_predictions)

if __name__ == '__main__':
    log = logging.getLogger('werkzeug')
    log.setLevel(logging.ERROR)
    app.run(host='0.0.0.0', port=args.port)  # Start the Flask server
