# TensorFlowCoral.js

The Coral TPU Drivers and Shinobi Coral Plugin were successfully tested on a Google Coral M.2 Accelerator A+E key.

USB version of Coral TPU is intended for development, not production use.

## Required Coral TPU Drivers

The Installer will attempt to install the drivers for you. The drivers are for **Debian-based** Operating Systems, like Ubuntu 20.04.

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **Plugin Manager** tab.
3. In the listing select the latest **"Coral TPU"** plugin to Download.
4. Hit `Run Installer` for the newly downloaded plugin.
    - This plugin's installer requires a system reboot if you choose to install Coral TPU Drivers.
5. Once Installed click "Enable" and restart Shinobi.
6. Reopen the Plugin Manager and run the `Test Object Detector` command.
7. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)

Your test should look similar to this.

```
If "person" was detected then it worked.
{"type": "info", "data": "ready"}
Detected Objects!
person
person
person
{"type": "data", "data": [{"bbox": [143, 6, 358, 390], "class": "person", "score": 0.87890625}, {"bbox": [115, 241, 378, 513], "class": "person", "score": 0.7890625}, {"bbox": [-3, 203, 217, 504], "class": "person", "score": 0.66015625}], "time": 23.20685099999764}
#END_PROCESS
```

[Learn more about Downloading and Installing Plugins here](https://docs.shinobi.video/plugin/install)
