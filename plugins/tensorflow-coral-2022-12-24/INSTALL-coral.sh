#!/bin/bash
ARCH=$(uname -m)
if [ -x "$(command -v apt)" ]; then
    echo "Install Coral TPU Drivers?"
    echo "(y)es or (N)o"
    read installTheStuffHomie
    if [ "$installTheStuffHomie" = "y" ] || [ "$installTheStuffHomie" = "Y" ]; then
        echo "Installing Coral TPU Drivers..."
        echo "deb https://packages.cloud.google.com/apt coral-edgetpu-stable main" | sudo tee /etc/apt/sources.list.d/coral-edgetpu.list

        curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

        sudo apt-get update -y
        sudo apt-get install gasket-dkms libedgetpu1-std -y
        sudo sh -c "echo 'SUBSYSTEM==\"apex\", MODE=\"0660\", GROUP=\"apex\"' >> /etc/udev/rules.d/65-apex.rules"

        sudo groupadd apex

        sudo adduser $USER apex
        sudo apt-get install zlib1g-dev libjpeg-dev libpng-dev -y
        sudo apt-get install python3-pip -y
        pip3 install Cython
        pip3 install Pillow==9.5.0

        if [ "$ARCH" = "x86_64" ]; then
            # for x86/x64
            echo "x86/x64 tflite runtime Installing..."
            wget https://cdn.shinobi.video/installers/python3/2023-10-12/tflite_runtime-2.5.0.post1-cp310-cp310-linux_x86_64.whl
            pip3 install tflite_runtime-2.5.0.post1-cp310-cp310-linux_x86_64.whl
            rm tflite_runtime-2.5.0.post1-cp310-cp310-linux_x86_64.whl
        elif [ "$ARCH" = "aarch64" ]; then
            # for arm
            echo "AARCH64 tflite runtime Installing..."
            wget https://cdn.shinobi.video/installers/python3/2023-10-12/tflite_runtime-2.5.0-cp310-cp310-linux_aarch64.whl
            pip3 install tflite_runtime-2.5.0-cp310-cp310-linux_aarch64.whl
            rm tflite_runtime-2.5.0-cp310-cp310-linux_aarch64.whl
        else
            echo "Architecture : $ARCH"
            echo "Trying to install tflite runtime from apt..."
            sudo apt-get install python3-pycoral -y
            pip3 install --extra-index-url https://google-coral.github.io/py-repo/ pycoral
        fi
        # for both x86/x64 and arm
        wget https://cdn.shinobi.video/installers/python3/2023-10-12/pycoral-2.0.0-cp310-cp310-linux_x86_64.whl
        pip3 install pycoral-2.0.0-cp310-cp310-linux_x86_64.whl
        rm pycoral-2.0.0-cp310-cp310-linux_x86_64.whl

        chown root:apex /dev/apex_0 && chmod 777 /dev/apex_0
        echo "------------------------------"
        echo "Reboot is required. Do it now?"
        echo "------------------------------"
        echo "(y)es or (N)o. Default is No."
        read rebootTheMachineHomie
        if [ "$rebootTheMachineHomie" = "y" ] || [ "$rebootTheMachineHomie" = "Y" ]; then
            sudo reboot
        fi
    fi
else
    echo "Debian based systems can only install this driver. Try Ubuntu 22.04.3."
fi
