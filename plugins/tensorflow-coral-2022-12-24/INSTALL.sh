#!/bin/bash
DIR=`dirname $0`
echo "Getting coral object detection models..."
mkdir -p models
wget "https://cdn.shinobi.video/binaries/tensorflow/coral/models-2021-01-26/ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite"
mv ssd_mobilenet_v2_coco_quant_postprocess_edgetpu.tflite models/
wget "https://cdn.shinobi.video/binaries/tensorflow/coral/models-2021-01-26/plugins_tensorflow-coral_models_coco_labels.txt"
mv plugins_tensorflow-coral_models_coco_labels.txt coco_labels.txt
mv coco_labels.txt models/
echo "Models downloaded."

npm install yarn -g --unsafe-perm --force
npm install --unsafe-perm
if [ ! -e "./conf.json" ]; then
    echo "Creating conf.json"
    sudo cp conf.sample.json conf.json
else
    echo "conf.json already exists..."
fi
sh $DIR/INSTALL-coral.sh
echo "!!!IMPORTANT!!!"
echo "Run the Test Command to see if the installation was successful!"
