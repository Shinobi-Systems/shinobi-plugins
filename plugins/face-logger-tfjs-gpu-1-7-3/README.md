# Facial Recognition with Logging (Super Level)

This version of the TensorFlow Plugin has been officially tested with the following GPUs.

- NVIDIA RTX 3070 (~300 fps continuous detection possible)
- NVIDIA GTX 1660

The GPUs were running with NVIDIA Drivers v515, CUDNN v8, CUDA Toolkit 11.2 on Ubuntu 20.04 and Rocky 9

tfjs requires a minimum compute capability of `6.0` for NVIDIA GPUs. However this seems to run more freely than the tfjs Object Detector and may have similar flexibility like the Yolo V3 plugin.

This plugin is labelled as "GPU Required". However you may be able to run it on CPU with poor performance.

## Required NVIDIA Drivers for GPU Support

The "Run Installer" command does not offer Driver Installation. You must select Driver Installation manually from the Commands drop down for this plugin.

NVIDIA Drivers v515, CUDNN v8, CUDA Toolkit 11.2 Installers are offered for Ubuntu 20.04 and Rocky 9

## Downloading and Installing the Plugin

1. Login to your Shinobi's Superuser panel.
    - https://YOUR_SHINOBI/super
2. Open the **"Plugin Manager"** tab.
3. In the listing select the latest **"Facial Recognition (tfjs 1.7.3, GPU Required)"** plugin to Download.
4. Skip this for CPU Only or if you already did it before. Select **"Install NVIDIA Drivers v515, CUDNN v8, CUDA Toolkit 11.2 (Ubuntu 20.04 and Rocky 9)"**
5. Hit `Run Installer` for the newly downloaded plugin.
6. Once Installed click "Enable" and restart Shinobi.
7. Reopen the Plugin Manager and run the `Test Facial Recognition`.
8. Now go ahead and login to the main Dashboard to setup a Monitor with Object Detection!
    - https://YOUR_SHINOBI/
    - [Watch this video for a visual guide with some extra tips](https://youtu.be/Vk_5hlSQeV0?t=433)

Your test should look similar to this.

```
############################################
@tensorflow/tfjs-node(-gpu) module test for Facial Recognition (Face Detector)
GPU Test for Tensorflow Module
############################################
Loading https://cdn.shinobi.video/images/test/people.jpg
Detecting upon https://cdn.shinobi.video/images/test/people.jpg
Detected Faces!
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.955102801322937,
  _classScore: 0.955102801322937,
  _className: '',
  _box: Box {
    _x: 101.52355420589447,
    _y: 2.5959871801113454,
    _width: 56.04688918590546,
    _height: 86.05560803902813
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.9052636623382568,
  _classScore: 0.9052636623382568,
  _className: '',
  _box: Box {
    _x: 50.337227404117584,
    _y: 108.17081538673021,
    _width: 66.87774896621704,
    _height: 92.31492350737719
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.8850675821304321,
  _classScore: 0.8850675821304321,
  _className: '',
  _box: Box {
    _x: 236.2567913532257,
    _y: 92.5131472590382,
    _width: 53.96692943572998,
    _height: 79.22279926199369
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.8353421688079834,
  _classScore: 0.8353421688079834,
  _className: '',
  _box: Box {
    _x: 248.87554943561554,
    _y: 160.2565502714901,
    _width: 52.06780958175659,
    _height: 104.091796875
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.7873269319534302,
  _classScore: 0.7873269319534302,
  _className: '',
  _box: Box {
    _x: 435.3798108100891,
    _y: 28.720590541090086,
    _width: 68.88608407974243,
    _height: 124.78176495057046
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.7628810405731201,
  _classScore: 0.7628810405731201,
  _className: '',
  _box: Box {
    _x: 348.33809423446655,
    _y: 125.38023363040689,
    _width: 58.75919580459595,
    _height: 96.48157490033906
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.7468788623809814,
  _classScore: 0.7468788623809814,
  _className: '',
  _box: Box {
    _x: 311.73157596588135,
    _y: 31.297522133746106,
    _width: 41.923922538757324,
    _height: 66.36572739945129
  }
}
A Face Found!
FaceDetection {
  _imageDims: Dimensions { _width: 612, _height: 408 },
  _score: 0.5245451927185059,
  _classScore: 0.5245451927185059,
  _className: '',
  _box: Box {
    _x: 529.4348323345184,
    _y: 3.568465694304436,
    _width: 58.23252582550049,
    _height: 56.42605913000023
  }
}
Done https://cdn.shinobi.video/images/test/people.jpg
#END_PROCESS
```

[Learn more about Downloading and Installing Plugins here](https://docs.shinobi.video/plugin/install)
