//
// Shinobi - Face Plugin
// Copyright (C) 2016-2025 Moe Alam, moeiscool
//
// # Donate
//
// If you like what I am doing here and want me to continue please consider donating :)
// PayPal : paypal@m03.ca
//
// Base Init >>
const fs = require('fs');
const sharp = require('sharp');
var config = require('./conf.json')
var dotenv = require('dotenv').config()
var s
const {
  workerData
} = require('worker_threads');
const isWorker = workerData && workerData.ok === true;
const facesFolder = config.facesFolder || './faces'
const unknownFacesFolder = config.unknownFacesFolder || './unknownFaces'
if(isWorker){
    try{
        s = require('../pluginWorkerBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginWorkerBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'WORKER : Plugin start has failed. pluginBase.js was not found.')
        }
    }
}else{
    try{
        s = require('../pluginBase.js')(__dirname,config)
    }catch(err){
        console.log(err)
        try{
            s = require('./pluginBase.js')(__dirname,config)
        }catch(err){
            console.log(err)
            return console.log(config.plug,'Plugin start has failed. pluginBase.js was not found.')
        }
    }
}
// Base Init />>
// Face - Face Recognition Init >>
var weightLocation = __dirname + '/weights'
const canvas = require('canvas')
var tfjsSuffix = ''
switch(config.tfjsBuild){
    case'gpu':
        tfjsSuffix = '-gpu'
    break;
    case'cpu':
    break;
    default:
        try{
            require(`@tensorflow/tfjs-node`)
        }catch(err){
            console.log(err)
        }
    break;
}
var tf = require(`@tensorflow/tfjs-node${tfjsSuffix}`)
faceapi = require('face-api.js')

const { createCanvas, Image, ImageData, Canvas } = canvas
faceapi.env.monkeyPatch({ Canvas, Image, ImageData })
s.monitorLock = {}
const minConfidence = config.faceMinConfidence || 0.5
// Face - Face Recognition Init />>
var addAwaitStatements = async function(){
    s.detectObject = function(){}
    await faceapi.nets.ssdMobilenetv1.loadFromDisk(weightLocation)
    await faceapi.nets.faceLandmark68Net.loadFromDisk(weightLocation)
    await faceapi.nets.faceRecognitionNet.loadFromDisk(weightLocation)
    const faceDetectionNet = faceapi.nets.ssdMobilenetv1
    var faceDetectionOptions = new faceapi.SsdMobilenetv1Options({ minConfidence });
    var faces = []
    var labeledDescriptors = []
    var faceMatcher
    var facesLoaded = 0
    const faceImagesIndex = {}
    const faceImagesIndexArray = {}
    var faceDescriptorRefreshTimeout
    var alreadyCompiling = false
    var loadingUnknown = false
    if(!fs.existsSync(unknownFacesFolder)){
        fs.mkdirSync(unknownFacesFolder);
    }
    const isJpeg = (imageFile) => {
        imageFile.indexOf('.jpg') > -1 || imageFile.indexOf('.jpeg') > -1
    }
    const loadFaceNames = () => {
        if(!fs.existsSync(facesFolder)){
            fs.mkdirSync(facesFolder);
        }
        faces = fs.readdirSync(facesFolder).concat(fs.readdirSync(unknownFacesFolder))
    }
    loadFaceNames()
    const createNewFaceMatcherFromLoadedFaceDescriptors = () => {
        const cleanDescriptors = labeledDescriptors.filter(function (el) {
          return el != null;
        });
        console.log('createNewFaceMatcherFromLoadedFaceDescriptors --> DONE!',labeledDescriptors,labeledDescriptors.length,typeof labeledDescriptors)
        if(cleanDescriptors.length > 0)faceMatcher = new faceapi.FaceMatcher(cleanDescriptors)
        startDetecting()
    }
    const removeFaceImageFromMemory = (personName,imageFile) => {
        const thisFaceIndex = faces.indexOf(personName)
        if(thisFaceIndex === -1)return;
        if(faceImagesIndex[personName]){
            faceImagesIndex[personName][imageFile] = null
            faceImagesIndex[personName][getFilenamePrefix(imageFile) + '.faceDescriptor'] = null
            delete(faceImagesIndex[personName][imageFile])
            delete(faceImagesIndex[personName][getFilenamePrefix(imageFile) + '.faceDescriptor'])
        }
        const faceDescriptors = Object.values(faceImagesIndex[personName] || {})
        if(faceDescriptors.length > 0){
            console.log('faceDescriptors','make new')
            labeledDescriptors[thisFaceIndex] = new faceapi.LabeledFaceDescriptors(
                personName,
                faceDescriptors
            )
        }else{
            console.log('faceDescriptors','delete')
            labeledDescriptors.splice(thisFaceIndex, 1)
        }
    }
    const getFaceFolderPrefix = (personName) => {
        const isUnknownFace = (personName.indexOf('UNKN_') > -1)
        return isUnknownFace ? unknownFacesFolder + '/' : facesFolder + '/'
    }
    const moveFaceImageInFileSystem = (options,callback) => {
        const oldFaceFolderPrefix = getFaceFolderPrefix(options.personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(options.newPersonName)
        const oldImagePath = oldFaceFolderPrefix + options.personName + '/' + options.imageFile
        const newImagePath = newFaceFolderPrefix + options.newPersonName + '/' + options.newImageFile
        const oldFaceDescriptorPath = oldFaceFolderPrefix + options.personName + '/' + getFilenamePrefix(options.imageFile) + '.faceDescriptor'
        const newFaceDescriptorPath = newFaceFolderPrefix + options.newPersonName + '/' + getFilenamePrefix(options.newImageFile) + '.faceDescriptor'
        const fileExists = fs.existsSync(oldImagePath)
        if(fileExists){
            fs.readFile(oldFaceDescriptorPath,(err,data) => {
                fs.writeFile(newFaceDescriptorPath,data,() => {
                    fs.unlink(oldFaceDescriptorPath,() => {
                        fs.readFile(oldImagePath,(err,data) => {
                            fs.writeFile(newImagePath,data,() => {
                                fs.unlink(oldImagePath,() => {
                                    callback()
                                })
                            })
                        })
                    })
                })
            })
        }else{
            callback()
        }
    }
    const moveFaceFolderInFileSystem = (options,callback) => {
        var personName = options.personName//.replace(/FACE_/g,'UNKN_')
        // personName = personName.indexOf('UNKN_')
        if(personName.indexOf('UNKN_') > -1){
            newPersonName = newPersonName.replace(/UNKN_/g,'FACE_')
        }else if(options.copyToUnknownFaces){
            newPersonName = newPersonName.indexOf('FACE_') > -1 ? newPersonName.replace(/FACE_/g,'UNKN_') : 'UNKN_' + newPersonName
        }
        const oldFaceFolderPrefix = getFaceFolderPrefix(personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(newPersonName)
        const oldFolderPath = oldFaceFolderPrefix + personName
        const newFolderPath = newFaceFolderPrefix + newPersonName
        const fileExists = fs.existsSync(oldFolderPath)
        if(fileExists){
            fs.readFile(oldFolderPath,(err,data) => {
                fs.writeFile(newFolderPath,data,() => {
                    fs.unlink(oldFolderPath,() => {
                        callback()
                    })
                })
            })
        }else{
            callback()
        }
    }
    const moveFaceImageInMemory = (options) => {
        const personName = options.personName
        const imageFile = options.imageFile
        const newPersonName = options.newPersonName
        const newImageFile = options.newImageFile
        const oldFaceIndex = faces.indexOf(personName)
        const newFaceIndex = faces.indexOf(newPersonName)
        if(oldFaceIndex === -1)return;
        if(faceImagesIndex[personName]){
            const imageFileFaceDescriptor = getFilenamePrefix(imageFile) + '.faceDescriptor'
            try{
                loadImageFaceDescriptor(newPersonName,newImageFile, () => {
                    faceImagesIndex[personName][imageFile] = null
                    faceImagesIndex[personName][imageFileFaceDescriptor] = null
                    delete(faceImagesIndex[personName][imageFile])
                    delete(faceImagesIndex[personName][imageFileFaceDescriptor])
                    const faceDescriptors = Object.values(faceImagesIndex[personName] || {})
                    if(faceDescriptors.length > 0){
                        console.log('faceDescriptors','make new')
                        labeledDescriptors[oldFaceIndex] = new faceapi.LabeledFaceDescriptors(
                            personName,
                            faceDescriptors
                        )
                    }else{
                        console.log('faceDescriptors','delete')
                        labeledDescriptors[oldFaceIndex] = null
                    }
                    buildFaceMatcherLabelFromDescriptors(newPersonName)
                    createNewFaceMatcherFromLoadedFaceDescriptors()
                })
            }catch(err){
                console.log(err)
            }
        }
    }
    const moveFaceFolderInMemory = (options,callback) => {
        const personName = options.personName
        const newPersonName = options.newPersonName
        const oldFaceFolderPrefix = getFaceFolderPrefix(personName)
        const newFaceFolderPrefix = getFaceFolderPrefix(newPersonName)
        fs.readdir(oldFaceFolderPrefix + personName,(err,files) => {
            if(files){
                var fileList = []
                files.forEach((fileName) => {
                    if(isJpeg(fileName)){
                        fileList.push(fileName)
                        const faceOptions = {
                            personName: personName,
                            imageFile: fileName,
                            newPersonName: newPersonName,
                            newImageFile: fileName
                        }
                        moveFaceImageInMemory(faceOptions)
                    }
                })
            }
            callback(fileList)
        })

    }
    const getFilenamePrefix = (imageFile) => {
        const fileParts = imageFile.split('.')
        delete(fileParts[fileParts.length - 1])
        const fileName = fileParts.filter((value) => {return !!value}).join('.')
        return fileName
    }
    const writeFaceDescriptorFile = (personName,faceDescriptor,fileName,callback) => {
        console.log(personName,fileName)
        const faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}`
        const fileNamePrefix = getFilenamePrefix(fileName)
        const fileNameDescriptor = `${fileNamePrefix}.faceDescriptor`
        fs.writeFile(`${faceFolder}/${personName}/${fileNameDescriptor}`,faceToArrayString(faceDescriptor),'utf8',(err) => {
            if(err){
                console.log(`Failed to Save Face Descriptor`, new Date())
                console.log(err)
                console.log(new Error())
            }
            if(callback)callback(err,fileNameDescriptor)
        })
    }
    const loadImageFaceDescriptor = (personName,imageFile,callback,parsedDescriptor) => {
            const faceDescriptorFilename = getFilenamePrefix(imageFile) + '.faceDescriptor'
            if(!faceImagesIndex[personName])faceImagesIndex[personName] = {};
            if(faceImagesIndex[personName][faceDescriptorFilename])return;
            const faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}/${personName}/`
            const getFromFile = () => {
                faceImagesIndex[personName][faceDescriptorFilename] = arrayStringToFace(fs.readFileSync(faceFolder + faceDescriptorFilename))
                if(callback)callback()
            }
            if(parsedDescriptor){
                faceImagesIndex[personName][faceDescriptorFilename] = parsedDescriptor
                if(callback)callback()
            }else if(imageFile.indexOf('.faceDescriptor') > -1){
                getFromFile()
            }else{
                fs.stat(faceFolder + faceDescriptorFilename,(err,stats) => {
                    if(stats){
                        getFromFile()
                    }else{
                        var image = new Image;
                        image.onload = function() {
                            faceapi
                              .detectSingleFace(image)
                              .withFaceLandmarks()
                              .withFaceDescriptor()
                              .then((singleResult) => {
                                  if (!singleResult) {
                                      const error = `no faces found in image ${imageFile}`
                                      if(callback)callback(error)
                                      return
                                  }
                                  faceImagesIndex[personName][faceDescriptorFilename] = singleResult.descriptor
                                  writeFaceDescriptorFile(personName,singleResult.descriptor,imageFile)
                                  if(callback)callback()
                              })
                              .catch((error) => {
                                  if(callback)callback(error)
                              })
                        }
                        image.src = fs.readFileSync(faceFolder + imageFile)
                    }
                })
            }
    }
    const buildFaceMatcherLabelFromDescriptors = (personName) => {
        const thisFaceIndex = faces.indexOf(personName)
        faceImagesIndexArray[personName] = Object.keys(faceImagesIndex[personName])
        labeledDescriptors[thisFaceIndex] = new faceapi.LabeledFaceDescriptors(
            personName,
            Object.values(faceImagesIndex[personName])
        )
    }
    const createAllFaceDescriptors = (forceRecompilation,callback) => {
        if(alreadyCompiling)return console.log('Blocked Recompilation...');
        alreadyCompiling = true
        console.log('Starting Recompilation...')
        const doRecompilation = forceRecompilation && forceRecompilation.length > 0
        faceMatcher = null
        facesLoaded = 0
        const completeFaceCheck = () => {
            ++facesLoaded
            console.log('facesLoaded ---> +1',facesLoaded)
            if(facesLoaded >= faces.length){
                console.log('completeFaceCheck ---> createNewFaceMatcherFromLoadedFaceDescriptors')
                createNewFaceMatcherFromLoadedFaceDescriptors()
                alreadyCompiling = false;
                if(callback){
                    console.log('completeFaceCheck ---> callback')
                    callback()
                }
            }
        }
        if(faces.length === 0)completeFaceCheck()
        faces.forEach(function(personName){
            var thisFaceIndex = faces.indexOf(personName)
            if(thisFaceIndex === -1){
                thisFaceIndex = faces.length
                faces.push(personName)
            }
            if(!faceImagesIndex[personName])faceImagesIndex[personName] = {}
            var faceFolder = `${personName.indexOf('UNKN_') > -1 ? unknownFacesFolder : facesFolder}/` + personName + '/'
            var imageList = fs.readdirSync(faceFolder)
            var foundImages = []
            var foundChange = false
            imageList.forEach(function(imageFile,number){
                const faceDescriptorFile = `${getFilenamePrefix(imageFile)}.faceDescriptor`
                if(!faceImagesIndex[personName][imageFile] && !faceImagesIndex[personName][faceDescriptorFile]){
                    if(isJpeg(imageFile)){
                        foundChange = true
                        try{
                            if(fs.statSync(faceFolder + faceDescriptorFile)){
                                foundImages.push(faceDescriptorFile)
                            }else{
                                foundImages.push(imageFile)
                            }
                        }catch(err){
                            foundImages.push(imageFile)
                        }
                    }else if(imageFile === faceDescriptorFile){
                        foundChange = true
                        foundImages.push(imageFile)
                    }
                }
            })
            console.log('foundChange : ',personName, foundChange)
            if(!foundChange){
                console.log('No Changes Found for : ' + personName)
                completeFaceCheck()
            }else{
                console.log('Loading : ' + personName)
                var faceChanged = false
                foundImages.forEach(function(imageFile,number){
                    const completeImageCheck = () => {
                        if(number === foundImages.length - 1){
                            console.log('Loaded : ' + personName)
                            const newKeys = Object.keys(faceImagesIndexArray[personName] || {})
                            if(faceChanged || faceImagesIndexArray[personName] !== newKeys || doRecompilation && forceRecompilation.indexOf(personName)){
                                buildFaceMatcherLabelFromDescriptors(personName)
                            }
                            completeFaceCheck()
                        }
                    }
                    if(!faceImagesIndex[personName][imageFile]){
                        faceChanged = true;
                        loadImageFaceDescriptor(personName,imageFile,(err) => {
                            completeImageCheck()
                        })
                    }else{
                        completeImageCheck()
                    }
                })
            }
        })
    }
    const recompileFaceDescriptors = (forceRecompilation,noCancellation,callback) => {
        if(faceDescriptorRefreshTimeout){
            if(noCancellation){
                console.log('No Cancelled')
                return
            }
            console.log('Cancelling previous recompilation request...')
        }
        console.log('Recompiling Face Descriptors...')
        clearTimeout(faceDescriptorRefreshTimeout)
        faceDescriptorRefreshTimeout = setTimeout(()=>{
            faceDescriptorRefreshTimeout = null
            createAllFaceDescriptors(forceRecompilation,callback)
        },10000)
    }
    const faceToArrayString = (faceDescriptor) => {
        return JSON.stringify(Array.from(faceDescriptor))
    }
    const arrayStringToFace = (faceString) => {
        return Float32Array.from(JSON.parse(faceString))
    }
    const foundUnknownFace = async (frameBuffer,faceDescriptor,matrix) => {
        if(alreadyCompiling === true || faceDescriptorRefreshTimeout || loadingUnknown === true)return;
        loadingUnknown = true
        const currentTime = (new Date()).getTime()
        const newFaceId = `UNKN_${currentTime}`
        faces.push(newFaceId)
        try{
            const facePath = `${unknownFacesFolder}/${newFaceId}`
            fs.mkdir(`${facePath}`,async (err) => {
                writeFaceDescriptorFile(newFaceId,faceDescriptor,`${currentTime}.newUnknownFace`,(err,faceDescriptorFilename) => {
                    if(!err){
                        loadImageFaceDescriptor(newFaceId,faceDescriptorFilename, () => {
                            buildFaceMatcherLabelFromDescriptors(newFaceId)
                            createNewFaceMatcherFromLoadedFaceDescriptors()
                            loadingUnknown = false
                        },faceDescriptor)
                    }
                })
                var objectCutOut = await sharp(frameBuffer).extract({
                    width: parseInt(matrix.width),
                    height: parseInt(matrix.height),
                    left: parseInt(matrix.x),
                    top: parseInt(matrix.y)
                }).toBuffer()
                fs.writeFile(`${facePath}/${currentTime}.jpg`,objectCutOut,(err) => {
                    if(err)console.log(err)
                })
            })
        }catch(err){
            console.log(err)
        }
    }
    var startDetecting = function(){
        console.log('Ready to Detect Faces')
        s.detectObject = function(buffer,d,tx,frameLocation){
            var detectStuff = function(frameBuffer,callback){
                try{
                    var startTime = new Date()
                    var image = new Image;
                    image.onload = async function() {
                        faceapi.detectAllFaces(image, faceDetectionOptions)
                        .withFaceLandmarks()
                        .withFaceDescriptors()
                        .then((data) => {
                            if(data && data[0]){
                                if(faceMatcher){
                                    data.forEach(fd => {
                                        var bestMatch = faceMatcher.findBestMatch(fd.descriptor)
                                        fd.detection.tag = bestMatch.toString()
                                    })
                                }
                                var endTime = new Date()
                                var matrices = []
                                try{
                                    var imgHeight = data[0].detection._imageDims._height
                                    var imgWidth = data[0].detection._imageDims._width
                                }catch(err){
                                    var imgHeight = data[0]._detection._imageDims._height
                                    var imgWidth = data[0]._detection._imageDims._width
                                }
                                data.forEach(function(box){
                                    var v = box.detection || box._detection
                                    var tag,confidence
                                    if(v.tag){
                                        var split = v.tag.split('(')
                                        tag = split[0].trim()
                                        if(tag === 'unknown'){
                                            tag = 'UNKNOWN FACE'
                                            foundUnknownFace(frameBuffer,box.descriptor,v._box)
                                        }
                                        if(split[1]){
                                            confidence = split[1].replace(')','')
                                        }else{
                                            confidence = v._score
                                        }
                                    }else{
                                        tag = 'UNKNOWN FACE'
                                        confidence = v._score
                                        foundUnknownFace(frameBuffer,box.descriptor,v._box)
                                    }
                                    matrices.push({
                                      id: tag,
                                      x: v._box.x,
                                      y: v._box.y,
                                      width: v._box.width,
                                      height: v._box.height,
                                      tag: tag,
                                      confidence: v._score,
                                    })
                                })
                                if(matrices.length > 0){
                                    tx({
                                        f:'trigger',
                                        id:d.id,
                                        ke:d.ke,
                                        details:{
                                            plug:config.plug,
                                            name:'face',
                                            reason:'object',
                                            matrices:matrices,
                                            imgHeight: imgHeight,
                                            imgWidth: imgWidth,
                                            ms: endTime - startTime
                                        },
                                    })
                                }
                            }
                        })
                        .catch((err) => {
                            console.log(err)
                        })
                    }
                    image.src = frameBuffer;
                }catch(err){
                    s.monitorLock[d.ke+d.id] = false
                    console.log(err)
                }
            }
            if(frameLocation){
                fs.readFile(frameLocation,function(err,buffer){
                    if(!err){
                        detectStuff(buffer)
                    }
                    fs.unlink(frameLocation,function(){

                    })
                })
            }else{
                detectStuff(buffer)
            }
        }
    }
    if(faces.length === 0){
        startDetecting()
    }else{
        createAllFaceDescriptors()
    }
    // add websocket handlers
    var sendData = () => {

    }
    const onSocketEvent = (d) => {
        switch(d.f){
            case'moveFaceFolder':
                const moveFaceFolderOptions = {
                    personName: d.personName,
                    newPersonName: d.newPersonName,
                }
                moveFaceFolderInFileSystem(moveFaceFolderOptions,() => {
                    moveFaceFolderInMemory(moveFaceFolderOptions,(fileList) => {
                        sendData('callback',d.callbackId)
                    })
                })
            break;
            case'moveFaceImage':
                const moveFaceOptions = {
                    personName: d.personName,
                    imageFile: d.imageFile,
                    newPersonName: d.newPersonName,
                    newImageFile: d.newImageFile,
                }
                moveFaceImageInFileSystem(moveFaceOptions,() => {
                    moveFaceImageInMemory(moveFaceOptions)
                })
            break;
            case'deleteFaceImage':
                removeFaceImageFromMemory(d.personName,d.imageFile)
            break;
            case'recompileFaceDescriptors':
                faces = d.faces
                unknownFaces = d.unknownFaces
                recompileFaceDescriptors(d.forceRecompile,true)
            break;
        }
    }
    if(isWorker){
        sendData = (type,data) => {
            parentPort.postMessage([type,data])
        }
        const {
            parentPort
        } = require('worker_threads');
        parentPort.on('message',onSocketEvent)
    }else{
        // add websocket handlers
        const io = s.getWebsocket()
        if(config.mode === 'host'){
            io.on('connection', function (cn) {
                sendData = (type,data) => {
                    cn.emit(type,data)
                }
                cn.on('f',onSocketEvent)
            })
        }else{
            sendData = (type,data) => {
                io.emit(type,data)
            }
            io.on('f',onSocketEvent)
        }
    }
}
addAwaitStatements()
