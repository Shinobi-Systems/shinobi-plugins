# Use Node.js 20 Bullseye Slim as the base image
FROM node:20-bullseye-slim

# Set an argument for the plugin directory, default to "tensorflow-4-1-0"
ARG PLUGIN_DIR=tensorflow-4-1-0
ARG ADDITIONAL_PACKAGES=
ARG PLUGIN_KEY=

# Set the working directory
WORKDIR /usr/src/app

RUN apt-get update && apt-get install -y build-essential git curl jq ${ADDITIONAL_PACKAGES}
RUN apt install sudo -y
COPY plugins ./plugins

# Set the environment variable PLUGIN_DIR which can be overridden at container run time
ENV PLUGIN_DIR=${PLUGIN_DIR}

# Copy the required files from the Shinobi repository into the specified plugin directory
ADD https://gitlab.com/Shinobi-Systems/Shinobi/-/raw/master/plugins/pluginBase.js plugins/${PLUGIN_DIR}/
ADD https://gitlab.com/Shinobi-Systems/Shinobi/-/raw/master/plugins/pluginCheck.js plugins/${PLUGIN_DIR}/
ADD https://gitlab.com/Shinobi-Systems/Shinobi/-/raw/master/tools/modifyConfigurationForPlugin.js tools/
# Change directory to the selected plugin directory
WORKDIR /usr/src/app/plugins/${PLUGIN_DIR}

RUN if [ -f INSTALL.sh ]; then \
        printf 'y\n' | sh INSTALL.sh; \
    elif [ -f ../tools/INSTALL.sh ]; then \
        printf 'y\n' | sh ../tools/INSTALL.sh; \
    else \
        npm install; \
    fi
RUN npm install moment@2.29.4 express@4.17.1 node-fetch@2.6.7 form-data@4.0.0

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
